describe "「有効/無効化」チェックボックス", ->
  beforeEach ->
    loadFixtures("container.html")
    @checkbox = document.querySelector("#{EventButton.SELECTORS.button("action_field:disable")}[for='person_children_attributes_1-action_field-member']")
    @event = document.createEvent("HTMLEvents")
    @event.initEvent("change", true, true)

  describe "をチェックした場合", ->
    beforeEach ->
      @checkbox.checked = true
      @checkbox.dispatchEvent(@event)

    it "は、メンバーを無効状態にすること", ->
      expect("#person_children_attributes_1-action_field-member").toHaveClass(ActionField.Member.MODE.DISABLED)
      fields = document.querySelectorAll("[name^='person[children_attributes][1]'],.event_button[for='person_children_attributes_1-action_field-member']")
      Array.prototype.forEach.call fields, (field)->
        unless field.matches(EventButton.SELECTORS.button("action_field:disable"))
          expect(field).toBeDisabled()

    it "は、「有効/無効化」チェックボックスを無効化しないこと", ->
      expect("#person_children_attributes_1-action_field-member #{EventButton.SELECTORS.button("action_field:disable")}").not.toBeDisabled()

  describe "をアンチェックした場合", ->
    beforeEach ->
      document.getElementById("person_children_attributes_1-action_field-member").classList.add(ActionField.Member.MODE.DISABLED)
      document.getElementById("person_children_attributes_1_name").disabled = true
      document.querySelector(".event_button[for='person_children_attributes_1-action_field-member']").disabled = true
      @checkbox.checked = false
      @checkbox.dispatchEvent(@event)

    it "は、メンバーを有効状態にすること", ->
      expect("#person_children_attributes_1-action_field-member").not.toHaveClass(ActionField.Member.MODE.DISABLED)
      fields = document.querySelectorAll("[name^='person[children_attributes][1]'],.event_button[for='person_children_attributes_1-action_field-member']")
      Array.prototype.forEach.call fields, (field)->
        expect(field).not.toBeDisabled()
