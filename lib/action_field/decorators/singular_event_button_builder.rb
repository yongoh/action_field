module ActionField
  module Decorators
    class SingularEventButtonBuilder < EventButtonBuilder

      # @return [ActiveSupport::SafeBuffer] 「新規作成」イベントボタン
      def create_button(*args, **options, &block)
        template_event_button(:create, *args, options, &block)
      end

      alias_method :_create_button, :create_button

      # @return [ActiveSupport::SafeBuffer] 「削除」イベントボタン
      def remove_button(*args, **options, &block)
        options.fetch(:for){|key| options[key] = decorator.member_id }
        remove_event_button(:remove, *args, **options, &block)
      end

      alias_method :_remove_button, :remove_button
    end
  end
end
