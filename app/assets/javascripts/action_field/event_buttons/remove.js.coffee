# メンバーの「削除」イベントリスナー
document.addEventButtonListener "click", "action_field:remove", class: "ActionField.EventButton", (button)->
  if elm = button.elementOfAction()
    new ActionField.Member(elm).remove(destroy: button.isDestroy())
