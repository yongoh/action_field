require 'rails_helper'

describe ActionField::ApplicationHelper do
  describe "#action_field" do
    let(:member_proc){
      proc do |f|
        h.concat h.content_tag(:span, "(´･ω･｀)", class: "shobon")
        h.concat f.text_field(:name)
        h.concat f.number_field(:height)
        h.concat f.remove_button
      end
    }

    shared_examples_for "with a empty container" do
      it "は、空のコンテナを返すこと" do
        is_expected.to have_tag(".action_field-container"){
          without_tag(".action_field-member")
        }
      end
    end

    let(:template){ subject.slice(/data-template="(.+?)"/, 1).gsub("&quot;", "'") }
    let(:new_person){ build(:person) }

    shared_examples_for "without template event_button" do
      it "は、新規メンバー用テンプレートを持つイベントボタンを含まないHTMLを返すこと" do
        is_expected.not_to have_tag(".event_button[template]")
      end
    end

    shared_examples_for "with template event_button" do
      it "は、新規メンバー用テンプレートを持つイベントボタンを含むHTMLを返すこと" do
        expect(template).to have_tag(".action_field-member", with: {"data-object-name" => object_name}, count: 1){
          without_tag("input", with: {type: "hidden"})
          with_tag("span.shobon", text: "(´･ω･｀)")
          with_text_field("#{object_name}[name]", new_person.name)
          with_number_field("#{object_name}[height]", new_person.height)
          with_tag(".event_button", with: {"data-event" => "action_field:remove", for: "#{h.send(:sanitize_to_id, object_name)}-action_field-member"})
        }
      end
    end

    context "オプション`:collection`に`true`を渡した場合 (default)" do
      shared_examples_for "with menu" do
        it "は、複数コンテナ用のイベントボタンを含むメニュー要素を返すこと" do
          is_expected.to have_tag(".action_field-menu#foo_bar-action_field-menu"){
            without_tag(".event_button", with: {"data-event" => "action_field:add"})
            with_tag(".event_button", with: {"data-event" => "action_field:clear"})
          }
        end
      end

      context "第2引数を渡さない場合" do
        subject do
          h.action_field("foo[bar]", &member_proc)
        end

        it_behaves_like "with a empty container"
        it_behaves_like "with menu"
      end

      context "第2引数にレコード群を渡した場合" do
        subject do
          h.action_field("foo[bar]", people, &member_proc)
        end

        let(:people){ create_list(:person, 3) }

        it "は、渡したレコード群と同じ数のメンバー要素を含むコンテナを返すこと" do
          is_expected.to have_tag("ul.action_field-container#foo_bar-action_field-container", with: {"data-object-name" => "foo[bar]"}){
            people.each_with_index do |person, i|
              with_tag("li.action_field-member#foo_bar_#{i}-action_field-member", with: {"data-object-name" => "foo[bar][#{i}]"}, count: 1){
                with_hidden_field("foo[bar][#{i}][id]", person.id)
                with_hidden_field("foo[bar][#{i}][_destroy]", "false")
                with_tag("span.shobon", text: "(´･ω･｀)")
                with_text_field("foo[bar][#{i}][name]", person.name)
                with_number_field("foo[bar][#{i}][height]", person.height)
                with_tag(".event_button", with: {"data-event" => "action_field:remove", for: "foo_bar_#{i}-action_field-member"})
              }
            end

            with_tag(".action_field-member", count: people.size)
          }
        end

        it_behaves_like "with menu"
      end

      describe "オプション`:templates`" do
        context "を渡さない場合" do
          subject do
            h.action_field("foo[bar]", &member_proc)
          end

          it_behaves_like "without template event_button"
        end

        context "にレコードの配列を渡した場合" do
          subject do
            h.action_field("foo[bar]", templates: [new_person], &member_proc)
          end

          let(:object_name){ "foo[bar][0]" }
          it_behaves_like "with template event_button"
        end
      end
    end

    context "オプション`:collection`に`false`を渡した場合" do
      shared_examples_for "with menu" do
        it "は、単数コンテナ用のイベントボタンを含むメニュー要素を返すこと" do
          is_expected.to have_tag(".action_field-menu#foo_bar-action_field-menu"){
            without_tag(".event_button", with: {"data-event" => "action_field:create"})
            with_tag(".event_button", with: {"data-event" => "action_field:remove"})
          }
        end
      end

      context "第2引数を渡さない場合" do
        subject do
          h.action_field("foo[bar]", collection: false, &member_proc)
        end

        it_behaves_like "with a empty container"
        it_behaves_like "with menu"
      end

      context "第2引数にレコードを渡した場合" do
        subject do
          h.action_field("foo[bar]", person, collection: false, &member_proc)
        end

        let(:person){ create(:person) }

        it "は、渡したレコードの値を持つメンバー要素を含むコンテナを返すこと" do
          is_expected.to have_tag("ul.action_field-container#foo_bar-action_field-container", with: {"data-object-name" => "foo[bar]"}){
            with_tag("li.action_field-member#foo_bar-action_field-member", with: {"data-object-name" => "foo[bar]"}, count: 1){
              with_hidden_field("foo[bar][id]", person.id)
              with_hidden_field("foo[bar][_destroy]", "false")
              with_tag("span.shobon", text: "(´･ω･｀)")
              with_text_field("foo[bar][name]", person.name)
              with_number_field("foo[bar][height]", person.height)
              with_tag(".event_button", with: {"data-event" => "action_field:remove", for: "foo_bar-action_field-member"})
            }
          }
        end

        it_behaves_like "with menu"
      end

      describe "オプション`:templates`" do
        context "を渡さない場合" do
          subject do
            h.action_field("foo[bar]", collection: false, &member_proc)
          end

          it_behaves_like "without template event_button"
        end

        context "にレコードの配列を渡した場合" do
          subject do
            h.action_field("foo[bar]", collection: false, templates: [new_person], &member_proc)
          end

          let(:object_name){ "foo[bar]" }
          it_behaves_like "with template event_button"
        end
      end
    end
  end
end
