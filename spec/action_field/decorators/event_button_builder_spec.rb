require 'rails_helper'

describe ActionField::Decorators::EventButtonBuilder do
  let(:builder){ described_class.new(decorator) }
  let(:decorator){ double("FormDecorator", h: view_context, container_id: "my-container-id") }

  describe "#event_button" do
    subject{ builder.event_button(:hoge) }

    it "は、イベントボタンを返すこと" do
      is_expected.to have_tag(
        ".event_button",
        with: {"data-event" => "action_field:hoge", for: "my-container-id"},
      )
    end
  end

  describe "#template_event_button" do
    let(:template){ subject.slice(/data-template="(.+?)"/, 1).gsub("&quot;", "'") }

    shared_examples_for "with template" do
      it "は、テンプレート付きイベントボタンを返すこと" do
        is_expected.to have_tag(
          ".event_button[data-template]",
          with: {"data-event" => "action_field:hoge", for: "my-container-id"},
        )
      end
    end

    context "第2引数にHTML要素を渡した場合" do
      subject do
        builder.template_event_button(:hoge, html){ "( ﾟДﾟ)" }
      end

      let(:html){
        h.capture do
          h.concat h.content_tag(:div, "(´･ω･｀)", id: "shobon")
          h.concat h.content_tag(:span, "（＾ω＾）", class: "boon")
        end
      }

      it_behaves_like "with template"

      it "は、ブロックを実行しないこと" do
        expect{|block|
          builder.template_event_button(:hoge, html, &block)
        }.to yield_successive_args
      end

      describe "テンプレート" do
        it "は、渡した要素であること" do
          expect(template).to have_tag(":root"){
            with_tag("div#shobon", text: "(´･ω･｀)")
            with_tag("span.boon", text: "（＾ω＾）")
          }
        end

        it "は、ブロック内で作成した要素は含まないこと" do
          expect(template).not_to include("( ﾟДﾟ)")
        end
      end
    end

    context "第2引数に`nil`を渡した場合" do
      subject do
        builder.template_event_button(:hoge, nil)
      end

      it "は、テンプレートを持たないイベントボタンを返すこと" do
        is_expected.to have_tag(
          ".event_button:not([data-template])",
          with: {"data-event" => "action_field:hoge", for: "my-container-id"},
        )
      end

      it "は、ブロックを実行しないこと" do
        expect{|block|
          builder.template_event_button(:hoge, nil, &block)
        }.to yield_successive_args
      end
    end

    context "第2引数にそれ以外のオブジェクトを渡した場合" do
      subject do
        builder.template_event_button(:hoge, arg)
      end

      let(:arg){ double("Argument") }

      before do
        allow(decorator).to receive(:members_for).and_return(
          h.capture do
            h.concat h.content_tag(:div, "(´･ω･｀)", id: "shobon")
            h.concat h.content_tag(:span, "（＾ω＾）", class: "boon")
          end
        )
      end

      it_behaves_like "with template"

      it "は、引数を`#decorator.members_for`に委譲すること" do
        expect(decorator).to receive(:members_for).with(equal(arg))
        builder.template_event_button(:hoge, arg)
      end

      it "は、ブロックを`#decorator.members_for`に委譲すること" do
        member_proc = proc{}
        allow(decorator).to receive(:members_for){|&block|
          expect(block).to equal(member_proc)
        }
        builder.template_event_button(:hoge, arg, &member_proc)
      end

      describe "テンプレート" do
        it "は、`#decorator.members_for`の結果であること" do
          expect(template).to have_tag(":root"){
            with_tag("div#shobon", text: "(´･ω･｀)")
            with_tag("span.boon", text: "（＾ω＾）")
          }
        end
      end
    end

    describe "オプション`:prepend`" do
      context "に`true`を渡した場合" do
        subject do
          builder.template_event_button(:hoge, nil, prepend: true)
        end

        it "は、prepend指標を持つイベントボタン要素を返すこと" do
          is_expected.to have_tag(".event_button.action_field-prepend", with: {"data-event" => "action_field:hoge"})
        end
      end

      context "に`false`を渡した場合" do
        subject do
          builder.template_event_button(:hoge, nil, prepend: false)
        end

        it "は、prepend指標を持たないイベントボタン要素を返すこと" do
          is_expected.to have_tag(".event_button:not(.action_field-prepend)", with: {"data-event" => "action_field:hoge"})
        end
      end
    end
  end

  describe "#remove_event_button" do
    subject{ builder.remove_event_button(:hoge) }

    it "は、イベントボタンを返すこと" do
      is_expected.to have_tag(".event_button", with: {"data-event" => "action_field:hoge", for: "my-container-id"})
    end

    context "オプション`:destroy_record`に`true`を渡した場合 (default)" do
      it "は、レコード削除指標付きのイベントボタンを返すこと" do
        is_expected.to have_tag(".event_button.action_field-destroy_record", with: {"data-event" => "action_field:hoge"})
      end
    end

    context "オプション`:destroy_record`に`false`を渡した場合" do
      subject do
        builder.remove_event_button(:hoge, destroy_record: false)
      end

      it "は、レコード削除指標無しのイベントボタンを返すこと" do
        is_expected.to have_tag(".event_button:not(.action_field-destroy_record)", with: {"data-event" => "action_field:hoge"})
      end
    end
  end
end
