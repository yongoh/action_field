describe "「新規作成」イベントボタンをクリックした場合", ->
  beforeEach ->
    loadFixtures("container.html")
    appendLoadFixtures("singular_menu.html")
    @button = document.querySelector(EventButton.SELECTORS.button("action_field:create"))

  itBehavesLikeCreate = ->
    it "は、テンプレートから生成した新しいメンバー要素をコンテナに入れること", ->
      expect("#person_children_attributes-action_field-container").toContainElement("div#new_record")

    it "は、生成したメンバー要素のオブジェクト名を置換しないこと", ->
      expect("#new_record").toHaveData("object-name", "new_record[0]")

  itBehavesLikeEmpty = ->
    describe "コンテナが空の場合", ->
      beforeEach ->
        document.getElementById("person_children_attributes-action_field-container").innerHTML = ""
        @button.click()

      itBehavesLikeCreate.call(this)

  describe "コンテナにすでに要素が入っている場合", ->
    beforeEach (done)->
      @button.click()
      setTimeout((-> done()), 500)

    it "は、削除したメンバーの`id`および`_destroy`隠し入力欄を削除すること", ->
      expect("#person_children_attributes-action_field-container").not.toContainElement("#person_children_attributes_0_id")
      expect("#person_children_attributes-action_field-container").not.toContainElement("#person_children_attributes_0__destroy")
      expect("#person_children_attributes-action_field-container").not.toContainElement("#person_children_attributes_1_id")
      expect("#person_children_attributes-action_field-container").not.toContainElement("#person_children_attributes_1__destroy")
      expect("#person_children_attributes-action_field-container").toContainElement("#animal_children_attributes_2_id")
      expect("#person_children_attributes-action_field-container").toContainElement("#animal_children_attributes_2__destroy")

  itBehavesLikeEmpty.call(this)

  describe "ボタン要素に継承指標がある場合", ->
    beforeEach ->
      @button.classList.add(ActionField.EventButton.MODE.INHERIT)
      @oldMember = document.querySelector("#person_children_attributes_1-action_field-member")
      @button.click()
      @newMember = document.querySelector("#person_children_attributes-action_field-container > :last-child")

    it "は、生成したメンバー要素に古いメンバーの入力欄の値を引き継ぐこと", ->
      oldField = document.querySelector("[name='#{@oldMember.dataset.objectName}[name]']")
      newField = document.querySelector("[name='#{@newMember.dataset.objectName}[name]']")
      expect(newField.value).toBe(oldField.value)

    it "は、生成したメンバー要素に古いメンバーのレコードIDを引き継がないこと", ->
      expect(@newMember).toContainElement("[name='#{@newMember.dataset.objectName}[id]'][value='999']")

    it "は、生成したメンバー要素に古いメンバーのレコード削除判定を引き継がないこと", ->
      expect(@newMember).toContainElement("[name='#{@newMember.dataset.objectName}[_destroy]'][value='true']")

    itBehavesLikeEmpty.call(this)

  describe "ボタン要素にコピー指標がある場合", ->
    beforeEach ->
      @button.classList.add(ActionField.EventButton.MODE.COPY)
      @oldMember = document.querySelector("#person_children_attributes_1-action_field-member")
      @button.click()
      @newMember = document.querySelector("#person_children_attributes-action_field-container > :last-child")

    it "は、古いメンバーのコピーを作成すること", ->
      expect(@newMember).toEqual(ActionField.SELECTORS.MEMBER)
      newFields = @newMember.querySelectorAll("input")
      oldFields = @oldMember.querySelectorAll("input")
      Array.prototype.forEach.call newFields, (newField, i)=>
        oldFieldName = newField.name.replace(@newMember.dataset.objectName, @oldMember.dataset.objectName)
        if oldField = document.querySelector("[name='#{oldFieldName}']")
          expect(oldField).toHaveValue(newField.value)

    it "は、レコードIDフィールドを持たないメンバー要素を生成すること", ->
      expect(@newMember).not.toContainElement("[name$='[id]']")

    it "は、削除判定フィールドを持たないメンバー要素を生成すること", ->
      expect(@newMember).not.toContainElement("[name$='[_destroy]']")

    itBehavesLikeEmpty.call(this)
