class ApplicationMailer < ActionMailer::Base
  default from: 'from@example.com'
  form 'mailer'
end
