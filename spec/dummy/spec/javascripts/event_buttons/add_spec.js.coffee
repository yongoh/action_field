describe "「追加」イベントボタンをクリックした場合", ->
  beforeEach ->
    loadFixtures("container.html")
    appendLoadFixtures("collection_menu.html")
    @button = document.querySelector(EventButton.SELECTORS.button("action_field:add"))

  itBehavesLikeReplaceAttributes = ->
    it "は、生成したメンバー要素のオブジェクト名を一意のものに置換すること", ->
      members = document.querySelectorAll("#person_children_attributes-action_field-container > #{ActionField.SELECTORS.MEMBER}")
      Array.prototype.forEach.call members, (member)=>
        if member.id != @newMember.id
          expect(@newMember.dataset.objectName).not.toBe(member.dataset.objectName)
          if @newMember.id != "new_record"
            expect(@newMember.querySelector("input").name).not.toBe(member.querySelector("input").name)
            expect(@newMember.querySelector(EventButton.SELECTORS.button("action_field:remove")).getAttribute("for"))
              .not.toBe(member.querySelector(EventButton.SELECTORS.button("action_field:remove")).getAttribute("for"))

  itBehavesLikeAppendMember = ->
    it "は、新しいメンバー要素をコンテナの末尾に追加すること", ->
      members = document.querySelectorAll("#person_children_attributes-action_field-container > *")
      expect(members.length).toBe(4)
      expect(members[0]).toHaveId("person_children_attributes_0-action_field-member")
      expect(members[1]).toHaveId("person_children_attributes_1-action_field-member")
      expect(members[2]).toHaveId("animal_children_attributes_2-action_field-member")

  itBehavesLikePrependMember = ->
    it "は、テンプレートから生成した新しいメンバー要素をコンテナの先頭に追加すること", ->
      members = document.querySelectorAll("#person_children_attributes-action_field-container > *")
      expect(members.length).toBe(4)
      expect(members[1]).toHaveId("person_children_attributes_0-action_field-member")
      expect(members[2]).toHaveId("person_children_attributes_1-action_field-member")
      expect(members[3]).toHaveId("animal_children_attributes_2-action_field-member")

  describe "ボタン要素に先頭追加指標が無い場合", ->
    beforeEach ->
      @button.click()
      @newMember = document.querySelector("#person_children_attributes-action_field-container > :last-child")

    itBehavesLikeAppendMember.call(this)
    itBehavesLikeReplaceAttributes.call(this)

  describe "ボタン要素に先頭追加指標がある場合", ->
    beforeEach ->
      @button.classList.add(ActionField.EventButton.MODE.PREPEND)
      @button.click()
      @newMember = document.querySelector("#person_children_attributes-action_field-container > :first-child")

    itBehavesLikePrependMember.call(this)
    itBehavesLikeReplaceAttributes.call(this)

  describe "ボタン要素に継承指標がある場合", ->
    beforeEach ->
      @button.classList.add(ActionField.EventButton.MODE.INHERIT)

    itBehavesLikeInherit = ->
      it "は、生成したメンバー要素に継承元のメンバーの入力欄の値を引き継ぐこと", ->
        oldField = document.querySelector("[name='#{@oldMember.dataset.objectName}[name]']")
        newField = document.querySelector("[name='#{@newMember.dataset.objectName}[name]']")
        expect(newField.value).toBe(oldField.value)

      it "は、生成したメンバー要素に継承元のメンバーのレコードIDを引き継がないこと", ->
        expect(@newMember).toContainElement("[name='#{@newMember.dataset.objectName}[id]'][value='999']")

      it "は、生成したメンバー要素に継承元のメンバーのレコード削除判定を引き継がないこと", ->
        expect(@newMember).toContainElement("[name='#{@newMember.dataset.objectName}[_destroy]'][value='true']")

    describe "ボタン要素に先頭追加指標が無い場合", ->
      beforeEach ->
        @oldMember = document.querySelector("#person_children_attributes_1-action_field-member")
        @button.click()
        @newMember = document.querySelector("#person_children_attributes-action_field-container > :last-child")

      itBehavesLikeAppendMember.call(this)
      itBehavesLikeReplaceAttributes.call(this)
      itBehavesLikeInherit.call(this)

    describe "ボタン要素に先頭追加指標がある場合", ->
      beforeEach ->
        @button.classList.add(ActionField.EventButton.MODE.PREPEND)
        @oldMember = document.querySelector("#person_children_attributes-action_field-container > :first-child")
        @button.click()
        @newMember = document.querySelector("#person_children_attributes-action_field-container > :first-child")

      itBehavesLikePrependMember.call(this)
      itBehavesLikeReplaceAttributes.call(this)
      itBehavesLikeInherit.call(this)

    describe "コンテナが空の場合", ->
      beforeEach ->
        document.getElementById("person_children_attributes_0-action_field-member").remove()
        document.getElementById("person_children_attributes_1-action_field-member").remove()
        @button.click()

      it "は、テンプレートから生成した新しいメンバー要素をコンテナに追加すること", ->
        expect("#person_children_attributes-action_field-container").toContainElement("#new_record")

  describe "ボタン要素にコピー指標がある場合", ->
    beforeEach ->
      @button.classList.add(ActionField.EventButton.MODE.COPY)

    itbehavesLikeCopyMember = ->
      it "は、一つ前のメンバーのコピーを追加すること", ->
        expect(@newMember).toEqual(ActionField.SELECTORS.MEMBER)
        newFields = @newMember.querySelectorAll("input")
        oldFields = @oldMember.querySelectorAll("input")
        Array.prototype.forEach.call newFields, (newField, i)=>
          oldFieldName = newField.name.replace(@newMember.dataset.objectName, @oldMember.dataset.objectName)
          if oldField = document.querySelector("[name='#{oldFieldName}']")
            expect(oldField).toHaveValue(newField.value)

      it "は、レコードIDフィールドを持たないメンバー要素を生成すること", ->
        expect(@newMember).not.toContainElement("[name$='[id]']")

      it "は、削除判定フィールドを持たないメンバー要素を生成すること", ->
        expect(@newMember).not.toContainElement("[name$='[_destroy]']")

    describe "ボタン要素に先頭追加指標が無い場合", ->
      beforeEach ->
        @oldMember = document.querySelector("#person_children_attributes_1-action_field-member")
        @button.click()
        @newMember = document.querySelector("#person_children_attributes-action_field-container > :last-child")

      itBehavesLikeAppendMember.call(this)
      itBehavesLikeReplaceAttributes.call(this)
      itbehavesLikeCopyMember.call(this)

    describe "ボタン要素に先頭追加指標がある場合", ->
      beforeEach ->
        @button.classList.add(ActionField.EventButton.MODE.PREPEND)
        @oldMember = document.querySelector("#person_children_attributes-action_field-container > :first-child")
        @button.click()
        @newMember = document.querySelector("#person_children_attributes-action_field-container > :first-child")

      itBehavesLikePrependMember.call(this)
      itBehavesLikeReplaceAttributes.call(this)
      itbehavesLikeCopyMember.call(this)

    describe "コンテナが空の場合", ->
      beforeEach ->
        document.getElementById("person_children_attributes_0-action_field-member").remove()
        document.getElementById("person_children_attributes_1-action_field-member").remove()
        @button.click()

      it "は、テンプレートから生成した新しいメンバー要素をコンテナに追加すること", ->
        expect("#person_children_attributes-action_field-container").toContainElement("#new_record")
