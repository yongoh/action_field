# 「全削除」イベントリスナー
document.addEventButtonListener "click", "action_field:clear", class: "ActionField.EventButton", (button)->
  container = button.container()
  container.clear(destroy: button.isDestroy())
