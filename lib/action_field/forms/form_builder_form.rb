module ActionField
  module Forms
    class FormBuilderForm < Form

      # @return [ActiveModel::Model] `#fields_for`に渡す用の疑似レコード
      DummyRecord = Class.new.include(ActiveModel::Model).new

      # @!attribute [r] form_builder
      #   @return [ActionView::Helpers::FormBuilder] 関連元のフォームビルダー
      attr_reader :form_builder
      alias_method :f, :form_builder
      delegate :fields_for, to: :form_builder

      # @param [ActionView::Helpers::FormBuilder] form_builder 関連元のフォームビルダー
      # @param [String] record_name 関連名
      def initialize(form_builder, *args, **options, &block)
        @form_builder = form_builder
        super(*args, options, &block)
        options.fetch(:collection){ self.collection = association.reflection.collection? }
      end

      # @return [ActiveRecord::Associations::Association] 関連オブジェクト
      def association
        f.object.association(record_name)
      end

      def record_name=(value)
        super(value)
        # @note `#template_record`を渡してはポリモーフィック関連の場合にエラーが発生してしまうため疑似レコードを渡している
        @object_name = fields_for(record_name, DummyRecord, child_index: ""){|form| break form.object_name }.sub(/\[\]$/, "")
      end

      # @note ポリモーフィック関連には非対応
      def template_record
        association.reflection.build_association({})
      end

      def default_templates
        [template_record]
      end

      # @return [Decorators::FormDecorator] 自信のデコレータ
      def decorate(*args, &block)
        Decorators::FormDecorator.new(f.instance_variable_get(:@template), self, *args, &block)
      end
    end
  end
end
