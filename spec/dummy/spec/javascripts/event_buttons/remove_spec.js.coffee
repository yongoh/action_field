describe "「削除」イベントボタンをクリックした場合", ->
  beforeEach ->
    loadFixtures("container.html")

  itBehavesLikeRemoveMember = ->
    describe "アニメーション終了後", ->
      beforeEach (done)->
        @button.click()
        setTimeout((-> done()), 500)

      it "は、イベントボタンに対応するメンバー要素だけを削除すること", ->
        expect("#person_children_attributes_0-action_field-member").toExist()
        expect("#person_children_attributes_1-action_field-member").not.toExist()
        expect("#animal_children_attributes_2-action_field-member").toExist()

    describe "イベントボタンにレコード削除指標がある場合", ->
      beforeEach (done)->
        @button.click()
        setTimeout((-> done()), 500)

      it "は、削除したメンバーの`id`および`_destroy`隠し入力欄を削除しないこと", ->
        expect("#person_children_attributes-action_field-container").toContainElement("#person_children_attributes_1_id")
        expect("#person_children_attributes-action_field-container").toContainElement("#person_children_attributes_1__destroy")

      it "は、削除したメンバーの`_destroy`隠し入力欄の値に'true'をセットすること", ->
        expect("#person_children_attributes_1__destroy").toHaveValue("true")

    describe "イベントボタンにレコード削除指標が無い場合", ->
      beforeEach (done)->
        @button.classList.remove(ActionField.EventButton.MODE.DESTROY)
        @button.click()
        setTimeout((-> done()), 500)

      it "は、削除したメンバーの`id`および`_destroy`隠し入力欄を削除すること", ->
        expect("#person_children_attributes-action_field-container").not.toContainElement("#person_children_attributes_1_id")
        expect("#person_children_attributes-action_field-container").not.toContainElement("#person_children_attributes_1__destroy")

  describe "イベントボタンがメニュー要素の中にある場合", ->
    beforeEach ->
      @button = document.querySelector("#person_children_attributes_1-action_field-member #{EventButton.SELECTORS.button("action_field:remove")}")

    itBehavesLikeRemoveMember.call(this)

  describe "イベントボタンがメニュー要素の外にある場合", ->
    beforeEach ->
      appendLoadFixtures("collection_menu.html")
      @button = document.querySelector("menu #{EventButton.SELECTORS.button("action_field:remove")}")

    itBehavesLikeRemoveMember.call(this)
