require "action_field/engine"
require "action_field/forms/form"
require "action_field/forms/form_builder_form"
require "action_field/forms/form_helper_form"
require "action_field/member_form_builder"
require "action_field/decorators/form_decorator"
require "action_field/decorators/event_button_builder"
require "action_field/decorators/collection_event_button_builder"
require "action_field/decorators/singular_event_button_builder"
require "action_field/form_helper"

module ActionField
  class << self

    def sanitize_to_id(template, object_name, *args)
      [template.send(:sanitize_to_id, object_name), :action_field, *args].join("-")
    end
  end
end
