# 「新規作成」イベントリスナー
document.addEventButtonListener "click", "action_field:create", class: "ActionField.EventButton", (button)->
  container = button.container()

  # 新しいメンバーを作成
  newMembers = container.createMembers(button)

  # 既存のメンバーを削除（レコードを削除する機能は無い）
  container.clear(destroy: false)

  # 新しいメンバーをコンテナに追加
  newMembers.forEach (newMember)->
    container.add(newMember)
