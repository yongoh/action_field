require 'rails_helper'

object_name = "people[0]"

context "単数レコードの入力欄群がある場合", js: true do
  before do
    visit edit_singular_form_person_path(person)
  end

  let(:person){ create(:person) }

  subject do
    proc do
      sleep 0.5
      click_button "Save changes"
    end
  end

  shared_examples_for "successfully saved" do
    scenario "は、更新成功の通知を表示すること" do
      subject.call
      expect(page).to have_selector("#notice", text: "Person was successfully saved.")
    end
  end

  context "編集して送信した場合" do
    before do
      fill_in "#{object_name}[name]", with: "織田信長"
      fill_in "#{object_name}[height]", with: "156.3"
    end

    scenario "は、レコードを新規作成しないこと" do
      expect(subject).not_to change{ Person.count }
    end

    scenario "は、レコードの値を更新すること" do
      subject.call
      expect(person.reload).to have_attributes(name: "織田信長", height: 156.3)
    end

    it_behaves_like "successfully saved"
  end

  context "「削除」イベントボタンをクリックして送信した場合" do
    before do
      click_button "削除"
    end

    scenario "は、レコードを削除すること" do
      expect(subject).to change{ Person.count }.by(-1)
      expect(Person).not_to be_exist(person.id)
    end

    it_behaves_like "successfully saved"
  end

  context "「新規作成」イベントボタンをクリックして送信した場合" do
    before do
      click_button "新規作成"

      fill_in "#{object_name}[name]", with: "織田信長"
      fill_in "#{object_name}[height]", with: "156.3"
    end

    let(:new_person){ Person.order("created_at").last }

    scenario "は、送信した値を持つ関連先レコードを新規作成すること" do
      expect(subject).to change{ Person.count }.by(1)
      expect(new_person).not_to eq(person)
      expect(new_person).to have_attributes(
        name: "織田信長",
        height: 156.3,
      )
    end

    scenario "は、旧レコードを削除しないこと" do
      subject.call
      expect(Person).to be_exists(person.id)
    end

    it_behaves_like "successfully saved"
  end
end
