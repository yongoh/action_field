require 'rails_helper'

context "複数レコードの入力欄群が空の場合", js: true do
  before do
    visit collection_form_people_path
  end

  subject do
    proc do
      sleep 0.5
      click_button "Save changes"
    end
  end

  shared_examples_for "successfully saved" do
    scenario "は、更新成功の通知を表示すること" do
      subject.call
      expect(page).to have_selector("#notice", text: "Person was successfully saved.")
    end
  end

  context "何もせず送信した場合" do
    scenario "は、DBを変化させないこと" do
      expect(subject).not_to change{ Person.count }
    end

    it_behaves_like "successfully saved"
  end

  context "「追加」イベントボタンをクリックして" do
    before do
      click_button "追加"

      object_name = find(".action_field-member:last-child")["data-object-name"]
      fill_in "#{object_name}[name]", with: "織田信長"
      fill_in "#{object_name}[height]", with: "156.4"
    end

    context "送信した場合" do
      scenario "は、レコードを追加すること" do
        expect(subject).to change{ Person.count }.by(1)
          expect(Person.all).to match([
            have_attributes(name: "織田信長", height: 156.4),
          ])
      end

      it_behaves_like "successfully saved"
    end

    context "追加したメンバーを削除して送信した場合" do
      before do
        within ".action_field-member:last-child" do
          click_button "削除"
        end
      end

      scenario "は、レコードを追加も削除もしないこと" do
        expect(subject).not_to change{ Person.count }
      end

      it_behaves_like "successfully saved"
    end
  end
end
