module ActionField
  module Forms
    class Form
      class << self

        # @return [ActionField::Forms::Form] ActionFieldフォームオブジェクト
        def build(*args, &block)
          class_get(*args, &block).new(*args, &block)
        end

        # @return [Class<ActionField::Forms::Form>] ActionFieldフォームオブジェクトクラス
        def class_get(*args, &block)
          case args.first
          when ActionView::Base then FormHelperForm
          when ActionView::Helpers::FormBuilder then FormBuilderForm
          end
        end
      end

      # @!attribute [r] object_name
      #   @return [String] 入力欄の`name`属性値の一部
      # @!attribute [rw] record_name
      #   @return [String] 入力欄の`name`属性値の一部
      # @!attribute [rw] collection
      #   @return [Boolean] 複数用ActionFieldかどうか
      # @!attribute [rw] options
      #   @return [Hash] `#fields_for`に渡すオプション群のデフォルト
      attr_reader :object_name, :record_name
      attr_accessor :collection, :options
      alias_method :collection?, :collection

      def initialize(record_name, collection: true, **options)
        self.record_name = record_name
        self.collection = collection
        self.options = options
      end

      def record_name=(value)
        @object_name = @record_name = value.to_s
      end

      def members_for(*args, **options, &member_proc)
        options = {builder: MemberFormBuilder, include_id: false}.merge(self.options).merge(options)
        fields_for(record_name, *args, options, &member_proc)
      end

      def default_templates
        []
      end
    end
  end
end
