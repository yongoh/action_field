module ActionField
  module Decorators
    class FormDecorator

      # @!attribute [r] form
      #   @return [ActionField::Forms::Form] ActionFieldフォームオブジェクト
      # @!attribute [rw] tag_name
      #   @return [Symbol] メンバー要素のHTMLタグ名
      # @!attribute [rw] include_id
      #   @return [Boolean] メンバー要素内にレコードIDおよび削除判定隠し入力欄を生成するかどうか
      # @!attribute [rw] member_proc
      #   @return [Proc] メンバー要素の内容を作る手続き
      attr_reader :form
      attr_accessor :tag_name, :include_id, :member_proc

      # @param [ActionView::Base] template ビューコンテキスト
      def initialize(template, form, tag_name, include_id: true, &member_proc)
        @template, @form = template, form
        @tag_name, @member_proc = tag_name, member_proc
        self.include_id = include_id
      end

      # @param [Boolean] include_id レコードIDおよび削除判定隠し入力欄を生成するかどうか
      # @return [ActionView::SafeBuffer] メンバー要素
      # @see ActionField::Forms::Form#members_for
      def members_for(*args, include_id: self.include_id, **options, &member_proc)
        member_proc ||= self.member_proc

        form.members_for(*args, options) do |member_form|
          h.content_tag(
            self.tag_name,
            class: "action_field-member",
            id: ActionField.sanitize_to_id(@template, member_form.object_name, :member),
            data: {object_name: member_form.object_name},
          ) do
            if include_id && member_form.object.try(:persisted?)
              h.concat(member_form.hidden_field(:id))
              h.concat(member_form.hidden_field(:_destroy))
            end
            h.concat(h.capture(member_form, &member_proc))
          end
        end
      end

      # @param [Symbol] tag_name タグ名
      # @return [ActionView::SafeBuffer] コンテナ要素
      # @see #members_for
      def container(tag_name, *args, &member_proc)
        h.content_tag(
          tag_name,
          members_for(*args, &member_proc),
          class: "action_field-container",
          id: container_id,
          data: {object_name: form.object_name},
        )
      end

      # @param [Symbol] tag_name タグ名
      # @param [ActionField::Decorators::EventButtonBuilder,Class] builder イベントボタンビルダーもしくはそのクラス
      # @param [Array<ActiveSupport::SafeBuffer,ActiveRecord::Base>] templates メンバー要素のテンプレートもしくはそれを作るためのレコードの配列
      # @yield [builder] イベントボタン群を作成するブロック。省略可。
      # @yieldparam [ActionField::Decorators::EventButtonBuilder] イベントボタンビルダー
      # @return [ActiveSupport::SafeBuffer] メニュー要素
      def menu(tag_name = :menu, *args, builder: event_button_builder, templates: form.default_templates)
        builder = builder.new(self) if builder.respond_to?(:new)

        h.content_tag(
          tag_name,
          class: "action_field-menu",
          id: menu_id,
        ) do
          if block_given?
            yield(builder)
          else
            templates.each do |template|
              h.concat(builder._create_button(template, &member_proc))
            end
            h.concat(builder._remove_button)
          end
        end
      end

      # @return [ActionField::Decorators::EventButtonBuilder] イベントボタンビルダー
      def event_button_builder
        default_builder_class.new(self)
      end

      def default_builder_class
        if form.collection?
          CollectionEventButtonBuilder
        else
          SingularEventButtonBuilder
        end
      end

      # @return [String] コンテナ要素の`id`属性値
      def container_id
        ActionField.sanitize_to_id(@template, form.object_name, :container)
      end

      # @return [String] メニュー要素の`id`属性値
      def menu_id
        ActionField.sanitize_to_id(@template, form.object_name, :menu)
      end

      # @return [String] メンバー要素の`id`属性値
      def member_id
        ActionField.sanitize_to_id(@template, form.object_name, :member)
      end

      def h
        @template
      end
    end
  end
end
