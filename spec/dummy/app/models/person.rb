class Person < ApplicationRecord
  belongs_to :parent, class_name: "Person", required: false
  has_many :children, class_name: "Person", foreign_key: :parent_id
  has_many :grandchildren, through: :children, source: :children

  accepts_nested_attributes_for :parent, :children, allow_destroy: true

  validates :name, presence: true
end
