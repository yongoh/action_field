require 'rails_helper'

feature "入力欄の有効/無効化機能", js: true do
  before do
    visit edit_person_path(person)
  end

  let(:person){ create(:person, :with_children) }

  context "特定のレコードだけ無効化をチェックして" do
    before do
      fill_in "person[children_attributes][0][name]", with: "織田信忠"
      fill_in "person[children_attributes][0][height]", with: "156.1"
      fill_in "person[children_attributes][1][name]", with: "織田信雄"
      fill_in "person[children_attributes][1][height]", with: "156.2"
      fill_in "person[children_attributes][2][name]", with: "織田信孝"
      fill_in "person[children_attributes][2][height]", with: "156.3"

      check "person_children_attributes_1_disabling"
    end

    context "送信した場合" do
      before do
        click_button "Update Person"
      end

      scenario "は、チェックしたレコードだけ更新しないこと" do
        old_child = person.children[1]
        person.reload

        expect(person.children).to match([
          have_attributes(name: "織田信忠", height: 156.1),
          eql(old_child),
          have_attributes(name: "織田信孝", height: 156.3),
        ])
      end
    end

    context "もう一度チェックを外して送信した場合" do
      before do
        uncheck "person_children_attributes_1_disabling"
        click_button "Update Person"
      end

      scenario "は、全てのレコードを更新すること" do
        person.reload

        expect(person.children).to match([
          have_attributes(name: "織田信忠", height: 156.1),
          have_attributes(name: "織田信雄", height: 156.2),
          have_attributes(name: "織田信孝", height: 156.3),
        ])
      end
    end
  end
end
