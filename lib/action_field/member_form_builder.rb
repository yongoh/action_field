module ActionField
  class MemberFormBuilder < ActionView::Helpers::FormBuilder

    # @param [Boolean] destroy_record 削除したメンバーのレコードを削除するかどうか
    # @return [ActiveSupport::SafeBuffer] 「削除」イベントボタン
    def remove_button(*args, destroy_record: true, **options, &block)
      options.html_attribute_merge!(class: "action_field-destroy_record") if destroy_record
      options.fetch(:for){|key| options[key] = ActionField.sanitize_to_id(@template, object_name, :member) }
      @template.event_button("action_field:remove", *args, options, &block)
    end

    # @return [ActiveSupport::SafeBuffer] 有効/無効化チェックボックス
    def disabling_checkbox(method = :disabling, **options)
      options = {
        class: "event_button",
        data: {event: "action_field:disable"},
        for: ActionField.sanitize_to_id(@template, object_name, :member),
        name: nil,
        checked: false,
      }.html_attribute_merge(options)

      check_box(method, options)
    end
  end
end
