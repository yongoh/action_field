class CreatePeople < ActiveRecord::Migration[5.2]
  def change
    create_table :people do |t|
      t.string :name, null: false
      t.float :height
      t.boolean :male, null: false
      t.date :birth

      t.timestamps

      t.index :name, unique: true
    end
  end
end
