class PeopleController < ApplicationController
  before_action :set_person, only: [:show, :edit, :update, :destroy, :edit_singular_form]

  # GET /people
  def index
    @people = Person.all
  end

  # GET /people/1
  def show
  end

  # GET /people/new
  def new
    @person = Person.new
  end

  # GET /people/1/edit
  def edit
  end

  # POST /people
  def create
    @person = Person.new(person_params)

    if @person.save
      redirect_to @person, notice: 'Person was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /people/1
  def update
    if @person.update(person_params)
      redirect_to @person, notice: 'Person was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /people/1
  def destroy
    @person.destroy
    redirect_to people_url, notice: 'Person was successfully destroyed.'
  end

  # GET /people/new_singular_form
  def new_singular_form
  end

  # GET /people/edit_singular_form
  def edit_singular_form
  end

  # GET /people/collection_form
  def collection_form
    @people = Person.all
  end

  # POST /people/collection_save
  def collection_save
    @people = []
    params.require(:people).each do |index, record_params|
      person = record_params.has_key?(:id) ? Person.find(record_params[:id]) : Person.new
      if record_params.has_key?(:_destroy) && ActiveRecord::Type::Boolean.new.cast(record_params[:_destroy])
        person.destroy!
      else
        person.assign_attributes(record_params.permit(:name, :height, :male, :birth))
        @people << person
      end
    end rescue ActionController::ParameterMissing

    ActiveRecord::Base.transaction do
      if @people.all?(&:save)
        redirect_to people_url, notice: 'Person was successfully saved.'
      else
        render :collection_form
        raise ActiveRecord::Rollback
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_person
      @person = Person.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def person_params
      params.require(:person).permit(
        :name, :height, :male, :birth,
        parent_attributes: [:id, :name, :height, :male, :birth, :_destroy],
        children_attributes: [:id, :name, :height, :male, :birth, :_destroy],
      )
    end
end
