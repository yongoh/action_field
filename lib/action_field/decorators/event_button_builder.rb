require 'event_button/builder'

module ActionField
  module Decorators
    class EventButtonBuilder < EventButton::Builder

      # @!attribute [r] decorator
      #   @return [ActionField::Decorators::FormDecorator] デコレータオブジェクト
      attr_reader :decorator

      def initialize(decorator)
        super(decorator.h)
        @decorator = decorator
      end

      # @param [Symbol] name イベントボタン名
      # @param [Hash] options オプションおよびHTML属性
      # @option options [String] :for 対応するコンテナのID属性値
      # @return [ActiveSupport::SafeBuffer] コンテナを対象に操作するイベントボタン
      # @see EventButton#event_button
      def event_button(name, *args, **options, &block)
        options.fetch(:for){|key| options[key] = decorator.container_id }
        super("action_field:#{name}", *args, options, &block)
      end

      # @param [ActiveSupport::SafeBuffer,ActiveRecord::Base] template テンプレート用メンバー要素、もしくはそれを作るためのレコード
      # @option options [Boolean] :prepend 先頭に追加するイベントボタンに設定
      # @option options [Boolean] :inherit 一つ前のメンバーの値を継承するイベントボタンに設定
      # @option options [Boolean] :copy 一つ前のメンバーをコピーするイベントボタンに設定
      # @yield [mb] `template`がレコードの場合に`#members_for`に渡すブロック
      # @return [ActiveSupport::SafeBuffer] メンバーのテンプレート付きイベントボタン
      # @see #event_button
      def template_event_button(name, template, *args, **options, &member_proc)
        template = decorator.members_for(template, &member_proc) unless template.nil? || template.is_a?(String)
        options.html_attribute_merge!(data: {template: template})
        %i(prepend inherit copy).each do |mode|
          options.html_attribute_merge!(class: "action_field-#{mode}") if options.delete(mode)
        end
        event_button(name, *args, options)
      end

      # @option options [Boolean] :destroy_record 削除したメンバーのレコードを削除するかどうか
      # @return [ActiveSupport::SafeBuffer] メンバーの削除用イベントボタン
      # @see #event_button
      def remove_event_button(*args, **options, &block)
        options.html_attribute_merge!(class: "action_field-destroy_record") if options.fetch(:destroy_record, true)
        event_button(*args, options, &block)
      end
    end
  end
end
