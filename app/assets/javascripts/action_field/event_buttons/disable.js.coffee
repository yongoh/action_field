# 「有効/無効化」イベントリスナー
document.addEventButtonListener "change", "action_field:disable", (button)->
  member = new ActionField.Member(button.elementOfAction())
  member.disable(button.elementOfPower().checked)
  button.elementOfPower().disabled = false
