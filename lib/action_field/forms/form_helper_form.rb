module ActionField
  module Forms
    class FormHelperForm < Form

      delegate :fields_for, to: :h

      def initialize(template, *args, &block)
        @template = template
        super(*args, &block)
      end

      # @param [ActiveRecord::Base,Array<ActiveRecord::Base>] record_objects メンバーレコードもしくはその配列
      def members_for(record_objects = nil, *args, **options)
        if collection?
          h.capture do
            Array(record_objects).each_with_index do |record_object, i|
              h.concat(super(record_object, *args, options.merge(child_index: options[:child_index] || i)) do |form|
                form.object_name += "[#{form.index}]"
                yield(form)
              end)
            end
          end
        elsif record_objects
          super
        end
      end

      # @return [Decorators::FormDecorator] 自信のデコレータ
      def decorate(*args, &block)
        Decorators::FormDecorator.new(@template, self, *args, &block)
      end

      private

      def h
        @template
      end
    end
  end
end
