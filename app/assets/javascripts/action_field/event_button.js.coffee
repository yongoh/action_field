window.ActionField ?= {}

class ActionField.EventButton extends EventButton

  # @return [ActionField.Container] 対応するコンテナオブジェクト
  container: ->
    new ActionField.Container(@elementOfAction())

  # @return [ActionField.Member] 対応するメンバーオブジェクト
  member: ->
    new ActionField.Member(@elementOfAction())

  # @return [Array<ActionField.Member>] テンプレートから生成した新しいメンバー要素群
  createMembers: ->
    ActionField.Member.parse(@elementOfPower().dataset.template)

  # @return [Boolean] 先頭に追加するかどうか
  isPrepend: ->
    @elementOfPower().classList.contains(ActionField.EventButton.MODE.PREPEND)

  # @return [Boolean] 継承機能付きかどうか
  isInherit: ->
    @elementOfPower().classList.contains(ActionField.EventButton.MODE.INHERIT)

  # @return [Boolean] 既存のメンバーをコピーするかどうか
  isCopy: ->
    @elementOfPower().classList.contains(ActionField.EventButton.MODE.COPY)

  # @return [Boolean] レコードを削除するかどうか
  isDestroy: ->
    @elementOfPower().classList.contains(ActionField.EventButton.MODE.DESTROY)

ActionField.EventButton.MODE =
  # メンバーをコンテナの先頭に追加する
  PREPEND: "action_field-prepend"
  # 一つ前のメンバーの値を継承したメンバーを作成する
  INHERIT: "action_field-inherit"
  # 一つ前のメンバーをコピーする
  COPY: "action_field-copy"
  # 削除したメンバーのレコードを削除
  DESTROY: "action_field-destroy_record"
