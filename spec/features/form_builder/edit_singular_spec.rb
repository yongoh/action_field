require 'rails_helper'

object_name = "person[parent_attributes]"

context "単数関連先の入力欄群がある場合", js: true do
  before do
    visit edit_person_path(person)
  end

  let(:person){ create(:person, :with_parent) }

  subject do
    proc do
      sleep 0.5
      click_button "Update Person"
    end
  end

  shared_examples_for "successfully updated" do
    scenario "は、更新成功の通知を表示すること" do
      subject.call
      expect(page).to have_selector("#notice", text: "Person was successfully updated.")
    end
  end

  context "編集して送信した場合" do
    before do
      fill_in "person[name]", with: "織田信長"
      fill_in "#{object_name}[name]", with: "織田信秀"
      fill_in "#{object_name}[height]", with: "156.3"
    end

    scenario "は、関連元と関連先の値を更新すること" do
      subject.call
      expect(person.reload).to have_attributes(
        name: "織田信長",
        parent: have_attributes(name: "織田信秀", height: 156.3),
      )
    end

    scenario "は、参照を変更しないこと" do
      expect(subject).not_to change{ person.reload.parent_id }
    end

    it_behaves_like "successfully updated"
  end

  context "「削除」イベントボタンをクリックして送信した場合" do
    before do
      within "#parent" do
        click_button "削除"
      end
    end

    scenario "は、関連先レコードを削除すること" do
      expect(subject).to change{ Person.count }.by(-1)
      expect(Person).not_to be_exists(person.parent.id)
    end

    scenario "は、参照を削除すること" do
      expect(subject).to change{ person.reload.parent_id }.from(person.parent.id).to(nil)
    end

    it_behaves_like "successfully updated"
  end

  context "「新規作成」イベントボタンをクリックして送信した場合" do
    before do
      within "#parent" do
        click_button "新規作成"

        fill_in "#{object_name}[name]", with: "織田信秀"
        fill_in "#{object_name}[height]", with: "156.3"
      end
    end

    scenario "は、送信した値を持つ関連先レコードを新規作成すること" do
      expect(subject).to change{ Person.count }.by(1)
      expect(person.reload.parent).to eq(Person.order("created_at").last).and have_attributes(
        name: "織田信秀",
        height: 156.3,
      )
    end

    scenario "は、参照を変更すること" do
      expect(subject).to change{ person.reload.parent_id }
    end

    scenario "は、旧関連先レコードを削除しないこと" do
      subject.call
      expect(Person).to be_exists(person.parent_id)
    end

    it_behaves_like "successfully updated"
  end
end
