describe "ActionField.Container", ->
  beforeEach ->
    loadFixtures("container.html")
    @container = new ActionField.Container(document.getElementById("person_children_attributes-action_field-container"))

  describe "#add", ->
    beforeEach ->
      @newMember = new ActionField.Member(document.createElement("div"))

    describe "第2引数に`false`を渡した場合 (default)", ->
      it "は、渡した要素をコンテナの末尾に追加すること", ->
        @container.add(@newMember)
        expect(@container.element).toContainElement("#person_children_attributes_0-action_field-member")
        expect(@container.element).toContainElement("#person_children_attributes_1-action_field-member")
        expect(@container.element).toContainElement("#animal_children_attributes_2-action_field-member")
        expect("#person_children_attributes-action_field-container > :last-child").toEqual(@newMember.element)

    describe "オプション`prepend`に`true`を渡した場合", ->
      it "は、渡した要素をコンテナの先頭に追加すること", ->
        @container.add(@newMember, prepend: true)
        expect("#person_children_attributes-action_field-container > :first-child").toEqual(@newMember.element)
        expect(@container.element).toContainElement("#person_children_attributes_0-action_field-member")
        expect(@container.element).toContainElement("#person_children_attributes_1-action_field-member")
        expect(@container.element).toContainElement("#animal_children_attributes_2-action_field-member")

    it "は、イベントを発火させること", ->
      spy = jasmine.createSpy("events")
      @container.element.addEventListener(ActionField.EVENTS.CREATED.type, spy)
      @container.add(@newMember)
      expect(spy).toHaveBeenCalled()

    it "は、作成したメンバー要素をターゲットとしたイベントオブジェクトを発火させること", ->
      @container.element.addEventListener ActionField.EVENTS.CREATED.type, (event)=>
        expect(event.target).toEqual(@newMember.element)
      @container.add(@newMember)

  describe "#clear", ->
    describe "アニメーション終了後", ->
      beforeEach (done)->
        @container.clear()
        setTimeout((-> done()), 500)

      it "は、コンテナに対応する全てのメンバー要素を削除すること", ->
        expect(@container.element).not.toContainElement(ActionField.SELECTORS.memberIn("person[children_attributes]"))

      it "は、コンテナに対応しないメンバー要素を削除しないこと", ->
        expect(@container.element).toContainElement("#animal_children_attributes_2-action_field-member")

    it "は、イベントを発火させること", ->
      spy = jasmine.createSpy("events")
      @container.element.addEventListener(ActionField.EVENTS.REMOVED.type, spy)
      @container.clear()
      expect(spy).toHaveBeenCalled()

    it "は、削除したメンバー要素をターゲットとしたイベントオブジェクトを発火させること", ->
      @container.element.addEventListener ActionField.EVENTS.REMOVED.type, (event)->
        expect(event.target).toEqual(".action_field-member")
        expect(event.target).not.toEqual("#animal_children_attributes_2-action_field-member")
      @container.clear()

  describe "#members", ->
    it "は、コンテナに対応する全てのメンバーオブジェクトを返すこと", ->
      result = @container.members()

      expect(result.length).toBe(2)
      Array.prototype.forEach.call result, (member)->
        expect(member).toEqual(jasmine.any(ActionField.Member))
        expect(member.element).toEqual(ActionField.SELECTORS.memberIn("person[children_attributes]"))
