require 'rails_helper'

context "複数関連先の入力欄群がある場合", js: true do
  before do
    visit edit_person_path(person)
  end

  let(:person){ create(:person, :with_children) }

  subject do
    proc do
      sleep 0.5
      click_button "Update Person"
    end
  end

  shared_examples_for "successfully updated" do
    scenario "は、更新成功の通知を表示すること" do
      subject.call
      expect(page).to have_selector("#notice", text: "Person was successfully updated.")
    end
  end

  context "編集して送信した場合" do
    before do
      fill_in "person[name]", with: "織田信長"
      fill_in "person[children_attributes][0][name]", with: "織田信忠"
      fill_in "person[children_attributes][0][height]", with: "156.1"
      fill_in "person[children_attributes][1][name]", with: "織田信雄"
      fill_in "person[children_attributes][1][height]", with: "156.2"
      fill_in "person[children_attributes][2][name]", with: "織田信孝"
      fill_in "person[children_attributes][2][height]", with: "156.3"
    end

    scenario "は、関連元と関連先の値を更新すること" do
      subject.call
      expect(person.reload).to have_attributes(
        name: "織田信長",
        children: [
          have_attributes(name: "織田信忠", height: 156.1),
          have_attributes(name: "織田信雄", height: 156.2),
          have_attributes(name: "織田信孝", height: 156.3),
        ],
      )
    end

    it_behaves_like "successfully updated"
  end

  context "「削除」イベントボタンをクリックして送信した場合" do
    before do
      within "#children .action_field-member:nth-child(2)" do
        click_button "削除"
      end
    end

    scenario "は、関連先レコードを削除すること" do
      expect(subject).to change{ Person.count }.by(-1)
      expect(Person).to be_exists(person.children[0].id)
      expect(Person).not_to be_exists(person.children[1].id)
      expect(Person).to be_exists(person.children[2].id)
    end

    it_behaves_like "successfully updated"
  end

  context "「追加」イベントボタンをクリックして" do
    before do
      within "#children" do
        click_button "追加"

        object_name = find(".action_field-member:last-child")["data-object-name"]
        fill_in "#{object_name}[name]", with: "羽柴秀勝"
        fill_in "#{object_name}[height]", with: "156.4"
      end
    end

    context "送信した場合" do
      scenario "は、関連先を追加すること" do
        expect(subject).to change{ Person.count }.by(1)
        expect(Person.last).to have_attributes(name: "羽柴秀勝", height: 156.4, parent_id: person.id)
      end

      it_behaves_like "successfully updated"
    end

    context "追加したメンバーを削除して送信した場合" do
      before do
        within "#children .action_field-member:last-child" do
          click_button "削除"
        end
      end

      scenario "は、レコードを追加も削除もしないこと" do
        expect(subject).not_to change{ Person.count }
      end

      it_behaves_like "successfully updated"
    end
  end

  context "「全削除」イベントボタンをクリックした場合" do
    context "確認ダイアログの「OK」をクリックした場合" do
      before do
        within "#children" do
          page.accept_confirm "Are you sure?" do
            click_button "全削除"
          end
        end
      end

      scenario "は、全てのメンバーを削除状態にすること" do
        expect(page).to have_no_selector("#children .action_field-member")
        expect(page).to have_selector("#children input[name$='[_destroy]'][value='true']", count: person.children.size, visible: false)
      end

      context "送信した場合" do
        before do
          create_list(:person, 3, parent_id: person.id)
        end

        scenario "は、フォームに含まれていた関連先だけを削除すること" do
          subject.call

          expect(Person.count).to eq(4)
          expect(Person).to be_exists(person.id)
          person.children.each do |child|
            expect(Person).not_to be_exists(child.id)
          end
          person.reload.children.each do |child|
            expect(Person).to be_exists(child.id)
          end
        end

        it_behaves_like "successfully updated"
      end
    end

    context "確認ダイアログの「キャンセル」をクリックした場合" do
      before do
        within "#children" do
          page.dismiss_confirm "Are you sure?" do
            click_button "全削除"
          end
        end
      end

      scenario "は、全てのメンバーそのままにすること" do
        expect(page).to have_selector("#children .action_field-member", count: person.children.size)
        expect(page).to have_selector("#children input[name$='[_destroy]'][value='false']", count: person.children.size, visible: false)
      end
    end
  end
end
