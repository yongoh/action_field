module ActionField
  module FormHelper

    # @param [String] record_name 入力欄の`name`属性値の一部
    # @return [ActiveSupport::SafeBuffer] ActionField
    # @see ActionField::Forms::Form#render
    def action_field(record_name, *args, **options, &member_proc)
      menu_options = options.extract!(:templates)
      decorator_options = options.extract!(:include_id)
      decorator = Forms::Form.build(self, record_name, options).decorate(:li, decorator_options, &member_proc)

      decorator.h.capture do
        decorator.h.concat(decorator.container(:ul, *args))
        decorator.h.concat(decorator.menu(menu_options))
      end
    end
  end
end
