# 「追加」イベントリスナー
document.addEventButtonListener "click", "action_field:add", class: "ActionField.EventButton", (button)->
  container = button.container()

  # 新しいメンバーを作成
  newMembers = container.createMembers(button)

  newMembers.forEach (newMember)->

    # インデックスをユニークなものに書き換え
    newMember.replaceObjectName("#{container.objectName}[#{ActionField.randomIndex()}]")

    # 新しいメンバーをコンテナに追加
    container.add(newMember, prepend: button.isPrepend())
