Rails.application.routes.draw do
  mount JasmineRails::Engine => '/specs' if defined?(JasmineRails)
  resources :people do
    member do
      get 'edit_singular_form'
    end
    collection do
      get 'new_singular_form'
      get 'collection_form'
      post 'collection_save'
    end
  end
end
