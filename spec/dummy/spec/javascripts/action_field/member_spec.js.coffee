describe "ActionField.Member", ->
  beforeEach ->
    loadFixtures("container.html")
    @member = new ActionField.Member(document.getElementById("person_children_attributes_1-action_field-member"))
    @ancestor = document.querySelector("#person_children_attributes-action_field-container")

  describe "#getField", ->
    it "は、渡した文字列を`name`属性値の末尾の階層に持つ入力欄要素を返すこと", ->
      expect(@member.getField("id")).toEqual("input[name='person[children_attributes][1][id]']")

  describe "#remove", ->
    itHevesLikeRemoveMember = ->
      it "は、自身の要素を削除すること", ->
        expect("#person_children_attributes_1-action_field-member").not.toExist()

    itHevesLikeRemoveRecord = ->
      it "は、`id`および`_destroy`隠し入力欄を削除しないこと", ->
        expect("#person_children_attributes-action_field-container").toContainElement("#person_children_attributes_1_id")
        expect("#person_children_attributes-action_field-container").toContainElement("#person_children_attributes_1__destroy")

      it "は、`_destroy`隠し入力欄の値に'true'をセットすること", ->
        expect("#person_children_attributes_1__destroy").toHaveValue("true")

    itHevesLikeDoNotRemoveRecord = ->
      it "は、`id`および`_destroy`隠し入力欄を削除すること", ->
        expect("#person_children_attributes-action_field-container").not.toContainElement("#person_children_attributes_1_id")
        expect("#person_children_attributes-action_field-container").not.toContainElement("#person_children_attributes_1__destroy")

    describe "`_destroy`隠し入力欄の値が`false`の場合", ->
      describe "引数に`false`を渡した場合 (false)", ->
        beforeEach (done)->
          @member.remove()
          setTimeout((-> done()), 500)

        itHevesLikeRemoveMember.call(this)
        itHevesLikeDoNotRemoveRecord.call(this)

      describe "引数に`true`を渡した場合", ->
        beforeEach (done)->
          @member.remove(destroy: true)
          setTimeout((-> done()), 500)

        itHevesLikeRemoveMember.call(this)
        itHevesLikeRemoveRecord.call(this)

    describe "`_destroy`隠し入力欄の値が`true`の場合", ->
      beforeEach ->
        document.getElementById("person_children_attributes_1__destroy").value = true

      describe "引数に`false`を渡した場合 (false)", ->
        beforeEach (done)->
          @member.remove()
          setTimeout((-> done()), 500)

        itHevesLikeRemoveMember.call(this)
        itHevesLikeDoNotRemoveRecord.call(this)

      describe "引数に`true`を渡した場合", ->
        beforeEach (done)->
          @member.remove(destroy: true)
          setTimeout((-> done()), 500)

        itHevesLikeRemoveMember.call(this)
        itHevesLikeRemoveRecord.call(this)

    it "は、イベントを発火させること", ->
      spy = jasmine.createSpy("events")
      @ancestor.addEventListener(ActionField.EVENTS.REMOVED.type, spy)
      @member.remove()
      expect(spy).toHaveBeenCalled()

    it "は、削除したメンバー要素をターゲットとしたイベントオブジェクトを発火させること", ->
      @ancestor.addEventListener ActionField.EVENTS.REMOVED.type, (event)->
        expect(event.target).toEqual("#person_children_attributes_1-action_field-member")
      @member.remove()

  describe "#replaceObjectName", ->
    beforeEach ->
      @member.replaceObjectName("foo[bar]")

    it "は、属性`name`の値を置換すること", ->
      expect(@member.element).not.toContainElement("[name='person[children_attributes][1][id]']")
      expect(@member.element).toContainElement("[name='foo[bar][id]']")

    it "は、属性`data-object-name`の値を置換すること", ->
      expect(@member.element).not.toEqual("[data-object-name='person[children_attributes][1]']")
      expect(@member.element).toEqual("[data-object-name='foo[bar]']")

    it "は、属性`id`の値を置換すること", ->
      expect(@member.element).not.toEqual("#person_children_attributes_1-action_field-member")
      expect(@member.element).not.toContainElement("#person_children_attributes_1_id")
      expect(@member.element).toEqual("#foo_bar-action_field-member")
      expect(@member.element).toContainElement("#foo_bar_id")

    it "は、属性`for`の値を置換すること", ->
      expect(@member.element).not.toContainElement("[for='person_children_attributes_1-action_field-member']")
      expect(@member.element).toContainElement("[for='foo_bar-action_field-member']")

    it "は、`@objectName`の値を変更すること", ->
      expect(@member.objectName).toBe("foo[bar]")

  describe "#disable", ->
    beforeEach ->
      document.getElementById("person_children_attributes_0_name").disabled = true
      document.getElementById("person_children_attributes_1_name").disabled = true
      document.getElementById("animal_children_attributes_2_name").disabled = true
      document.querySelector(".event_button[for='animal_children_attributes_2-action_field-member']").disabled = true

    itHevesLikeNotChengeOthers = ->
      it "は、メンバーに対応しない入力欄を変化させないこと", ->
        expect("#person_children_attributes_0_name").toBeDisabled()
        expect("#person_children_attributes_0_height").not.toBeDisabled()
        expect("#animal_children_attributes_2_name").toBeDisabled()
        expect("#animal_children_attributes_2_height").not.toBeDisabled()

      it "は、メンバーに対応しないイベントボタンを変化させないこと", ->
        expect(".event_button[for='person_children_attributes_0-action_field-member']").not.toBeDisabled()
        expect(".event_button[for='animal_children_attributes_2-action_field-member']").toBeDisabled()

    describe "`true`を渡した場合", ->
      beforeEach ->
        @member.disable(true)

      it "は、メンバー要素に無効化指標をつけること", ->
        expect(@member.element).toHaveClass(ActionField.Member.MODE.DISABLED)

      it "は、メンバーに対応する入力欄を無効化すること", ->
        Array.prototype.forEach.call document.querySelectorAll("[name^='person[children_attributes][1]']"), (field)->
          expect(field).toBeDisabled()

      it "は、メンバーに対応するイベントボタンを無効化すること", ->
        Array.prototype.forEach.call document.querySelectorAll("[for='person_children_attributes_1-action_field-member']"), (field)->
          expect(field).toBeDisabled()

      itHevesLikeNotChengeOthers.call(this)

    describe "`false`を渡した場合", ->
      beforeEach ->
        document.getElementById("person_children_attributes_1-action_field-member").classList.add(ActionField.Member.MODE.DISABLED)
        document.querySelector(".event_button[for='person_children_attributes_1-action_field-member']").disabled = true
        @member.disable(false)

      it "は、メンバー要素から無効化指標をはずすこと", ->
        expect(@member.element).not.toHaveClass(ActionField.Member.MODE.DISABLED)

      it "は、メンバーに対応する入力欄を有効化すること", ->
        Array.prototype.forEach.call document.querySelectorAll("[name^='person[children_attributes][1]']"), (field)->
          expect(field).not.toBeDisabled()

      it "は、メンバーに対応するイベントボタンを有効化すること", ->
        Array.prototype.forEach.call document.querySelectorAll("[for='person_children_attributes_1-action_field-member']"), (field)->
          expect(field).not.toBeDisabled()

      itHevesLikeNotChengeOthers.call(this)

  describe "::parse", ->
    it "は、渡したテンプレート文字列からメンバーオブジェクト群を生成すること", ->
      template = document.getElementById("person_children_attributes_0-action_field-member").outerHTML
      result = ActionField.Member.parse(template)

      expect(result[0]).toEqual(jasmine.any(ActionField.Member))
      expect(result[0].element).toHaveId("person_children_attributes_0-action_field-member")
