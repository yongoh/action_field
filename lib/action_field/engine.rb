module ActionField
  class Engine < ::Rails::Engine

    initializer "include mix-in" do
      ActionView::Helpers::FormBuilder.include(FormHelper)
    end
  end
end
