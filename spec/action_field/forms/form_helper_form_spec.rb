require 'rails_helper'

describe ActionField::Forms::FormHelperForm do
  let(:form){ described_class.new(view_context, "foo[bar]") }

  describe "#members_for" do
    context "`#collection? == treu`の場合" do
      context "第1引数に`nil`を渡した場合 (default)" do
        it "は、ブロックを実行しないこと" do
          expect{|block| form.members_for(nil, &block) }.to yield_successive_args
        end
      end

      context "第1引数にレコードを渡した場合" do
        let(:person){ create(:person) }

        it "は、1回だけブロックを実行すること" do
          expect{|block| form.members_for(person, &block) }.to yield_successive_args(
            be_a(ActionField::MemberFormBuilder).and(have_attributes(object: equal(person))),
          )
        end
      end

      context "第1引数にレコードの配列を渡した場合" do
        let(:people){ create_list(:person, 3) }

        it "は、配列の長さだけブロックを実行すること" do
          expect{|block| form.members_for(people, &block) }.to yield_successive_args(
            *people.each_with_index.map do |person, i|
              be_a(ActionField::MemberFormBuilder).and(have_attributes(object: equal(person)))
            end,
          )
        end
      end

      describe "オプション`:child_index`" do
        let(:people){ create_list(:person, 3) }

        shared_examples_for "order index" do
          it "は、序数をインデックスに持つフォームビルダーをブロック引数に渡すこと" do
            expect(subject).to yield_successive_args(
              *people.each_with_index.map do |person, index|
                have_attributes(index: index, object_name: "foo[bar][#{index}]")
              end,
            )
          end
        end

        shared_examples_for "received index" do
          it "は、渡したオブジェクトをインデックスに持つフォームビルダーをブロック引数に渡すこと" do
            expect{|block| form.members_for(people, child_index: index, &block) }.to yield_successive_args(
              *people.map do
                have_attributes(index: index, object_name: "foo[bar][#{index}]")
              end,
            )
          end
        end

        context "を渡さない場合" do
          subject do
            proc{|block| form.members_for(people, &block) }
          end

          it_behaves_like "order index"
        end

        context "に`nil`を渡した場合" do
          subject do
            proc{|block| form.members_for(people, child_index: nil, &block) }
          end

          it_behaves_like "order index"
        end

        context "に`false`を渡した場合" do
          subject do
            proc{|block| form.members_for(people, child_index: false, &block) }
          end

          it_behaves_like "order index"
        end

        context "に`true`を渡した場合" do
          let(:index){ true }
          it_behaves_like "received index"
        end

        context "に空文字を渡した場合" do
          let(:index){ "" }
          it_behaves_like "received index"
        end

        context "に数値を渡した場合" do
          let(:index){ 123 }
          it_behaves_like "received index"
        end
      end
    end

    context "`#collection? == false`の場合" do
      before do
        allow(form).to receive(:collection?).and_return(false)
      end

      context "第1引数に`nil`を渡した場合 (default)" do
        it "は、ブロックを実行しないこと" do
          expect{|block| form.members_for(nil, &block) }.to yield_successive_args
        end
      end

      context "第1引数にレコードを渡した場合" do
        let(:person){ create(:person) }

        it "は、1回だけブロックを実行すること" do
          expect{|block| form.members_for(person, &block) }.to yield_successive_args(
            be_a(ActionField::MemberFormBuilder).and(have_attributes(object: equal(person))),
          )
        end
      end

      context "第1引数にレコードの配列を渡した場合" do
        let(:people){ create_list(:person, 3) }

        it "は、配列の長さだけブロックを実行すること" do
          expect{|block| form.members_for(people, &block) }.to yield_successive_args(
            be_a(ActionField::MemberFormBuilder).and(have_attributes(object: equal(people))),
          )
        end
      end
    end
  end
end
