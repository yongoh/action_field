module ActionField
  module Decorators
    class CollectionEventButtonBuilder < EventButtonBuilder

      # @return [ActiveSupport::SafeBuffer] 「追加」イベントボタン
      def add_button(*args, **options, &block)
        template_event_button(:add, *args, options, &block)
      end

      alias_method :_create_button, :add_button

      # @return [ActiveSupport::SafeBuffer] 「全削除」イベントボタン
      def clear_button(*args, **options, &block)
        options = {data: {confirm: "Are you sure?"}}.html_attribute_merge(options)
        remove_event_button(:clear, *args, **options, &block)
      end

      alias_method :_remove_button, :clear_button
    end
  end
end
