class RemoveIndexNameFromPeople < ActiveRecord::Migration[5.2]
  def change
    remove_index :people, :name
  end
end
