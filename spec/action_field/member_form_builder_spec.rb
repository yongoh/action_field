require 'rails_helper'

describe ActionField::MemberFormBuilder do
  let(:form){ described_class.new("foo[bar]", person, view_context, {}) }
  let(:person){ create(:person) }

  describe "#text_field" do
    it{ expect(form.text_field(:name)).to have_tag(":root"){ with_text_field("foo[bar][name]", person.name) } }
  end

  describe "#remove_button" do
    subject do
      form.remove_button
    end

    it "は、「削除」イベントボタンを返すこと" do
      is_expected.to have_tag(
        ".event_button.action_field-destroy_record",
        with: {"data-event" => "action_field:remove", for: "foo_bar-action_field-member"},
        text: "削除",
      )
    end

    context "オプション`:destroy_record`に`true`を渡した場合 (default)" do
      it "は、レコード削除指標付きの「削除」イベントボタンを返すこと" do
        is_expected.to have_tag(".event_button.action_field-destroy_record", with: {"data-event" => "action_field:remove"})
      end
    end

    context "オプション`:destroy_record`に`false`を渡した場合" do
      subject do
        form.remove_button(destroy_record: false)
      end

      it "は、レコード削除指標無しの「削除」イベントボタンを返すこと" do
        is_expected.to have_tag(".event_button:not(.action_field-destroy_record)", with: {"data-event" => "action_field:remove"})
      end
    end
  end

  describe "#disabling_checkbox" do
    context "キーワード引数`:checked`に`false`を渡した場合 (default)" do
      it "は、チェックされていない有効/無効化チェックボックスを返すこと" do
        expect(form.disabling_checkbox(checked: false)).to have_tag(
          "input.event_button#foo_bar_disabling:not([name]):not([checked])",
          with: {
            type: "checkbox",
            "data-event" => "action_field:disable",
            for: "foo_bar-action_field-member",
          },
        )
      end
    end

    context "キーワード引数`:checked`に`true`を渡した場合" do
      it "は、チェック済みの有効/無効化チェックボックスを返すこと" do
        expect(form.disabling_checkbox(checked: true)).to have_tag(
          "input.event_button#foo_bar_disabling:not([name])",
          with: {
            type: "checkbox",
            "data-event" => "action_field:disable",
            for: "foo_bar-action_field-member",
            checked: "checked",
          },
        )
      end
    end
  end
end
