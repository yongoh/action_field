window.ActionField ?= {}

class ActionField.Container

  # @!attribute [r] element
  #   @return [HTMLUlElement] コンテナ要素（メンバー要素の入れ物）
  # @!attribute [r] objectName
  #   @return [String] オブジェクト名（対応する入力欄の`name`属性値の前半）

  constructor: (element)->
    @element = element
    @objectName = @element.dataset.objectName

  # コンテナにメンバー要素を追加
  # @param [HTMLElement] newMember 追加するメンバー要素
  # @param [Hash] options オプション
  # @option options [Boolean] prepend `true`なら先頭、`false`なら末尾に追加する
  add: (newMember, options = {})->
    if options.prepend
      # コンテナの先頭に追加
      @element.insertBefore(newMember.element, @element.firstElementChild)
    else
      # コンテナの末尾に追加
      @element.appendChild(newMember.element)

    # アニメーション
    if typeof jQuery == "function"
      $(newMember.element).hide().slideDown("fast")

    # カスタムイベントを発火
    newMember.element.dispatchEvent(ActionField.EVENTS.CREATED)

  # コンテナに対応するメンバー要素を削除
  clear: (args...)->
    @members().forEach((member)-> member.remove(args...))

  # @return [Array<ActionField.Member>] コンテナに対応する全てのメンバーオブジェクト
  members: ->
    elms = @element.querySelectorAll(":scope > #{ActionField.SELECTORS.memberIn(@objectName)}")
    Array.prototype.map.call(elms, (elm)-> new ActionField.Member(elm))

  # @param [Hash] options オプション
  # @option options [Boolean] prepend `true`なら先頭、`false`なら末尾のメンバーを取得する
  # @return [ActionField.Member] コンテナに含まれるうち最後のメンバー
  last: (options = {})->
    members = @members()
    if options.prepend then members[0] else members[members.length - 1]

  # @param [ActionField.EventButton] button イベントボタンオブジェクト
  # @return [Array<ActionField.Member>] 新規作成もしくはコピーしたメンバー
  createMembers: (button)->

    # コピー・継承元メンバーを取得
    oldMember = @last(prepend: button.isPrepend())

    # 新しいメンバーを作成
    newMembers = if button.isCopy() && oldMember then [oldMember.clone()] else button.createMembers()

    newMembers.forEach (newMember)->

      # 値の継承処理
      if button.isInherit() && oldMember
        newMember.inherit(oldMember)

    newMembers
