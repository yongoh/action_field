FactoryBot.define do
  factory :person do
    name { %w(涼宮ハルヒ キョン 長門有希 朝比奈みくる 古泉一樹).sample }
    height { rand(150.0..190.0).round(1) }
    male { [true, false].sample }
    birth { Date.jd(rand(2433283..2458849)) }

    trait :with_parent do
      association :parent, factory: :person
    end

    trait :with_children do
      children{ create_list(:person, 3) }
    end
  end
end
