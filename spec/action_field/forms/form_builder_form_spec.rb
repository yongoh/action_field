require 'rails_helper'

describe ActionField::Forms::FormBuilderForm do
  let(:form_builder){ ActionView::Helpers::FormBuilder.new("foo[bar]", person, view_context, {}) }
  let(:person){ create(:person) }

  describe "#members_for" do
    context "`::new`の第2引数に複数関連の名前を渡した場合" do
      let(:form){ described_class.new(form_builder, "children") }

      context "第1引数にレコードの配列を渡した場合" do
        let(:people){ create_list(:person, 3) }

        it "は、配列の長さだけブロックを実行すること" do
          expect{|block| form.members_for(people, &block) }.to yield_successive_args(
            *people.map do |person|
              be_a(ActionField::MemberFormBuilder).and(have_attributes(object: equal(person)))
            end,
          )
        end
      end

      context "関連先が存在する場合" do
        let(:person){ create(:person, :with_children) }

        it "は、関連先レコードの数だけブロックを実行すること" do
          expect{|block| form.members_for(&block) }.to yield_successive_args(
            *person.children.map do |child|
              be_a(ActionField::MemberFormBuilder).and(have_attributes(object: equal(child)))
            end,
          )
        end
      end

      context "関連先が存在しない場合" do
        it "は、ブロックを実行しないこと" do
          expect{|block| form.members_for(&block) }.to yield_successive_args
        end
      end
    end

    context "`::new`の第2引数に単数関連の名前を渡した場合" do
      let(:form){ described_class.new(form_builder, "parent") }

      context "第1引数にレコードを渡した場合" do
        let(:other){ create(:person) }

        it "は、第1引数に渡したレコードのメンバー用カスタムフォームビルダーをブロック引数に渡すこと" do
          expect{|block| form.members_for(other, &block) }.to yield_successive_args(
            be_a(ActionField::MemberFormBuilder).and(have_attributes(object: equal(other))),
          )
        end
      end

      context "関連先が存在する場合" do
        let(:person){ create(:person, :with_parent) }

        it "は、関連先レコードのメンバー用カスタムフォームビルダーをブロック引数に渡すこと" do
          expect{|block| form.members_for(&block) }.to yield_successive_args(
            be_a(ActionField::MemberFormBuilder).and(have_attributes(object: equal(person.parent))),
          )
        end
      end

      context "関連先が存在しない場合" do
        it "は、ブロックを実行しないこと" do
          expect{|block| form.members_for(&block) }.to yield_successive_args
        end
      end
    end
  end
end
