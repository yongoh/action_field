class AddParentIdToPeople < ActiveRecord::Migration[5.2]
  def change
    change_table :people do |t|
      t.references :parent, index: true
    end
  end
end
