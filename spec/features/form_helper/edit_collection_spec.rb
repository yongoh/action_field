require 'rails_helper'

context "複数レコードの入力欄群がある場合", js: true do
  before do
    people
    visit collection_form_people_path
  end

  let(:people){ create_list(:person, 3) }

  subject do
    proc do
      sleep 0.5
      click_button "Save changes"
    end
  end

  shared_examples_for "successfully saved" do
    scenario "は、更新成功の通知を表示すること" do
      subject.call
      expect(page).to have_selector("#notice", text: "Person was successfully saved.")
    end
  end

  context "編集して送信した場合" do
    before do
      fill_in "people[0][name]", with: "織田信忠"
      fill_in "people[0][height]", with: "156.1"
      fill_in "people[1][name]", with: "織田信雄"
      fill_in "people[1][height]", with: "156.2"
      fill_in "people[2][name]", with: "織田信孝"
      fill_in "people[2][height]", with: "156.3"
    end

    scenario "は、レコードの値を更新すること" do
      subject.call
      expect(Person.all).to match([
        have_attributes(name: "織田信忠", height: 156.1),
        have_attributes(name: "織田信雄", height: 156.2),
        have_attributes(name: "織田信孝", height: 156.3),
      ])
    end

    it_behaves_like "successfully saved"
  end

  context "「削除」イベントボタンをクリックして送信した場合" do
    before do
      within ".action_field-member:nth-child(2)" do
        click_button "削除"
      end
    end

    scenario "は、そのレコードを削除すること" do
      expect(subject).to change{ Person.count }.by(-1)
      expect(Person).to be_exists(people[0].id)
      expect(Person).not_to be_exists(people[1].id)
      expect(Person).to be_exists(people[2].id)
    end

    it_behaves_like "successfully saved"
  end

  context "「追加」イベントボタンをクリックして" do
    before do
      click_button "追加"

      object_name = find(".action_field-member:last-child")["data-object-name"]
      fill_in "#{object_name}[name]", with: "羽柴秀勝"
      fill_in "#{object_name}[height]", with: "156.4"
    end

    context "送信した場合" do
      scenario "は、レコードを追加すること" do
        expect(subject).to change{ Person.count }.by(1)
        expect(Person.all).to match([
          *people.map{|person| eq(person) },
          have_attributes(name: "羽柴秀勝", height: 156.4),
        ])
      end

      it_behaves_like "successfully saved"
    end

    context "追加したメンバーを削除して送信した場合" do
      before do
        within ".action_field-member:last-child" do
          click_button "削除"
        end
      end

      scenario "は、レコードを追加も削除もしないこと" do
        expect(subject).not_to change{ Person.count }
      end

      it_behaves_like "successfully saved"
    end
  end

  context "「全削除」イベントボタンをクリックして送信した場合" do
    before do
      create_list(:person, 5)
      click_button "全削除"
    end

    scenario "は、フォームに含まれていたレコードだけを削除すること" do
      expect(subject).to change{ Person.count }.by(-3)
      people.each do |person|
        expect(Person).not_to be_exists(person.id)
      end
    end

    it_behaves_like "successfully saved"
  end
end
