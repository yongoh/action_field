require 'rails_helper'

describe ActionField::Decorators::FormDecorator do
  let(:decorator){ described_class.new(view_context, form, :li){ "(´･ω･｀)" } }
  let(:form){ double("Form", object_name: "foo[bar]") }

  describe "#members_for" do
    subject{ decorator.members_for }

    before do
      allow(form).to receive(:members_for){|&block| block.call(member_form_builder) }
      allow(member_form_builder).to receive(:hidden_field){|*args, &block| h.hidden_field(member_form_builder.object_name, *args, &block) }
    end

    let(:member_form_builder){ double("FormBuilder", object_name: "#{form.object_name}[baz]", object: build(:person)) }

    it "は、`#member_tag_name`をタグ名とするメンバー要素を返すこと" do
      is_expected.to have_tag("li.action_field-member")
    end

    it "は、メンバーの`object_name`の情報を持つメンバー要素を返すこと" do
      is_expected.to have_tag(
        ".action_field-member#foo_bar_baz-action_field-member",
        with: {"data-object-name": "foo[bar][baz]"},
      )
    end

    shared_examples_for "don't include id" do
      it "は、レコードIDおよび削除判定隠し入力欄を含まないHTMLを返すこと" do
        is_expected.to have_tag(":root"){
          without_tag("input")
        }
      end
    end

    context "オプション`:include_id`に`true`を渡した場合 (default)" do
      context "ブロック引数の`#object`が保存済みレコードを返す場合" do
        before do
          member_form_builder.object.save
        end

        it "は、レコードIDおよび削除判定隠し入力欄を含むメンバー要素を返すこと" do
          is_expected.to have_tag(".action_field-member"){
            with_hidden_field("foo[bar][baz][id]")
            with_hidden_field("foo[bar][baz][_destroy]")
          }
        end
      end

      context "ブロック引数の`#object`が未保存レコードを返す場合" do
        it_behaves_like "don't include id"
      end
    end

    context "オプション`:include_id`に`false`を渡した場合" do
      subject do
        decorator.members_for(include_id: false)
      end

      context "ブロック引数の`#object`が保存済みレコードを返す場合" do
        before do
          member_form_builder.object.save
        end

        it_behaves_like "don't include id"
      end

      context "ブロック引数の`#object`が未保存レコードを返す場合" do
        it_behaves_like "don't include id"
      end
    end

    it "は、引数を`form.members_for`に委譲すること" do
      args = 3.times.map{|n| double("Argument #{n}") }
      expect(form).to receive(:members_for).with(*args, {})
      decorator.members_for(*args)
    end

    context "ブロックを渡さない場合" do
      it "は、`#member_proc`のブロック引数に`form.members_for`のブロック引数を渡すこと" do
        expect{|block|
          decorator.member_proc = proc(&block)
          subject
        }.to yield_with_args(equal(member_form_builder))
      end

      it "は、`#members_for`の結果を内容に持つメンバー要素を返すこと" do
        is_expected.to have_tag(".action_field-member", text: "(´･ω･｀)")
      end
    end

    context "ブロックを渡した場合" do
      it "は、ブロック引数に`form.members_for`のブロック引数を渡すこと" do
        expect{|block|
          decorator.members_for(&block)
        }.to yield_with_args(equal(member_form_builder))
      end

      subject do
        decorator.members_for do
          h.concat(h.content_tag(:div, "(｀･ω･´)", id: "shobon"))
          h.concat(h.content_tag(:span, "（＾ω＾）", class: "boon"))
        end
      end

      it "は、ブロックの戻り値を内容に持つメンバー要素を返すこと" do
        is_expected.to have_tag(".action_field-member"){
          with_tag("div#shobon", text: "(｀･ω･´)", count: 1)
          with_tag("span.boon", text: "（＾ω＾）", count: 1)
          without_text("(´･ω･｀)")
        }
      end
    end
  end

  describe "#container" do
    subject{ decorator.container(:ul) }

    before do
      allow(decorator).to receive(:members_for).and_return("ヽ(`Д´)ﾉ")
    end

    it "は、第1引数をタグ名とするコンテナ要素を返すこと" do
      is_expected.to have_tag("ul.action_field-container")
    end

    it "は、`form.object_name`の情報を持つコンテナ要素を返すこと" do
      is_expected.to have_tag(
        ".action_field-container#foo_bar-action_field-container",
        with: {"data-object-name" => "foo[bar]"},
      )
    end

    it "は、`#members_for`の結果を内容に持つコンテナ要素を返すこと" do
      is_expected.to have_tag(".action_field-container", text: "ヽ(`Д´)ﾉ")
    end

    it "は、引数を`#members_for`に委譲すること" do
      args = 3.times.map{|n| double("Argument #{n}") }
      expect(decorator).to receive(:members_for).with(*args)
      decorator.container(:ul, *args)
    end

    it "は、ブロックを`#members_for`に委譲すること" do
      member_proc = proc{}
      allow(decorator).to receive(:members_for){|&block|
        expect(block).to equal(member_proc)
      }
      decorator.container(:ul, &member_proc)
    end
  end

  describe "#menu" do
    before do
      allow(form).to receive(:collection?).and_return(true)
      allow(form).to receive(:default_templates).and_return(3.times.map{ double("Template or record") })
    end

    let(:builder){ double("EventButtonBuilder") }

    context "第1引数を渡さない場合" do
      it "は、デフォルトのタグ名のメニュー要素を返すこと" do
        expect(decorator.menu{}).to have_tag("menu.action_field-menu#foo_bar-action_field-menu")
      end
    end

    context "第1引数にタグ名を渡した場合" do
      it "は、渡したタグ名のメニュー要素を返すこと" do
        expect(decorator.menu(:div){}).to have_tag("div.action_field-menu#foo_bar-action_field-menu")
      end
    end

    context "ブロックを渡さない場合" do
      before do
        allow(builder).to receive(:_create_button).and_return(h.content_tag(:div, "(・∀・)", class: "create"))
        allow(builder).to receive(:_remove_button).and_return(h.content_tag(:div, "(・A・)", class: "remove"))
      end

      context "オプション`:templates`を渡さない場合" do
        subject do
          decorator.menu(builder: builder)
        end

        it "は、`form.default_templates`が返す配列の数の`builder._create_button`の結果と、1つの`builder._remove_button`の結果を含むHTMLを返すこと" do
          is_expected.to have_tag(":root"){
            with_tag("div.create", text: "(・∀・)", count: form.default_templates.size)
            with_tag("div.remove", text: "(・A・)", count: 1)
          }
        end

        it "は、`form.default_templates`が返す配列のメンバーアイテムと`#member_proc`を`builder._create_button`に渡すこと" do
          allow(builder).to receive(:_create_button){|*args, &block| block.call(*args) }

          expect{|block|
            decorator.member_proc = block
            subject
          }.to yield_successive_args(*form.default_templates)
        end
      end

      context "オプション`:templates`に配列を渡した場合" do
        subject do
          decorator.menu(builder: builder, templates: templates)
        end

        let(:templates){ 5.times.map{ double("Template or record") } }

        it "は、渡した配列の数の`builder._create_button`の結果と、1つの`builder._remove_button`の結果を含むHTMLを返すこと" do
          is_expected.to have_tag(":root"){
            with_tag("div.create", text: "(・∀・)", count: templates.size)
            with_tag("div.remove", text: "(・A・)", count: 1)
          }
        end

        it "は、渡した配列のメンバーアイテムと`#member_proc`を`builder._create_button`に渡すこと" do
          allow(builder).to receive(:_create_button){|*args, &block| block.call(*args) }

          expect{|block|
            decorator.member_proc = block
            subject
          }.to yield_successive_args(*templates)
        end
      end
    end

    context "ブロックを渡した場合" do
      subject do
        decorator.menu do
          h.concat(h.content_tag(:div, "(｀･ω･´)", id: "shobon"))
          h.concat(h.content_tag(:span, "（＾ω＾）", class: "boon"))
        end
      end

      it "は、ブロック内で作成した内容を持つメニュー要素を返すこと" do
        is_expected.to have_tag(".action_field-menu"){
          with_tag("div#shobon", text: "(｀･ω･´)", count: 1)
          with_tag("span.boon", text: "（＾ω＾）", count: 1)
          without_text("(´･ω･｀)")
        }
      end

      context "オプション`:builder`を渡さない場合" do
        context "`form.collection? == true`の場合" do
          it "は、ブロック引数に複数用イベントボタンビルダーを渡すこと" do
            expect{|block| decorator.menu(&block) }.to yield_with_args(
              be_instance_of(ActionField::Decorators::CollectionEventButtonBuilder).and(have_attributes(decorator: equal(decorator))),
            )
          end
        end

        context "`form.collection? == false`の場合" do
          before do
            allow(form).to receive(:collection?).and_return(false)
          end

          it "は、ブロック引数に単数用イベントボタンビルダーを渡すこと" do
            expect{|block| decorator.menu(&block) }.to yield_with_args(
              be_instance_of(ActionField::Decorators::SingularEventButtonBuilder).and(have_attributes(decorator: equal(decorator))),
            )
          end
        end
      end

      context "オプション`:builder`にクラスを渡した場合" do
        let(:builder_class){ double("Class") }

        it "は、そのインスタンスを生成してブロック引数に渡すこと" do
          expect(builder_class).to receive(:new).and_return(builder).with(equal(decorator))
          expect{|block| decorator.menu(builder: builder_class, &block) }.to yield_with_args(equal(builder))
        end
      end

      context "オプション`:builder`にインスタンスを渡した場合" do
        it "は、渡したものをブロック引数に渡すこと" do
          expect{|block| decorator.menu(builder: builder, &block) }.to yield_with_args(equal(builder))
        end
      end
    end
  end
end
