require 'rails_helper'

object_name = "person"

context "単数レコードの入力欄群がない場合", js: true do
  before do
    visit new_singular_form_people_path
  end

  subject do
    proc do
      sleep 0.5
      click_button "Save changes"
    end
  end

  shared_examples_for "unchange records" do
    scenario "は、レコードを変化させないこと" do
      expect(subject).not_to change{ Person.count }
    end

    scenario "は、更新に失敗しフォーム画面に戻ること" do
      subject.call
      expect(page).to have_form("/people", "post")
      expect(page).to have_selector("#error_explanation")
    end
  end

  context "送信した場合" do
    it_behaves_like "unchange records"
  end

  context "「削除」イベントボタンをクリックして送信した場合" do
    before do
      click_button "削除"
    end

    it_behaves_like "unchange records"
  end

  context "「新規作成」イベントボタンをクリックして" do
    before do
      click_button "新規作成"

      fill_in "#{object_name}[name]", with: "織田信長"
      fill_in "#{object_name}[height]", with: "156.3"
    end

    context "編集して送信した場合" do
      scenario "は、レコードを新規作成すること" do
        expect(subject).to change{ Person.count }.by(1)
        expect(Person.last).to have_attributes(name: "織田信長", height: 156.3)
      end

      scenario "は、更新成功の通知を表示すること" do
        subject.call
        expect(page).to have_selector("#notice", text: "Person was successfully created.")
      end
    end

    context "「削除」イベントボタンをクリックして送信した場合" do
      before do
        click_button "削除"
      end

      it_behaves_like "unchange records"
    end
  end
end
