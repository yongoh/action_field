window.ActionField ?= {}

class ActionField.Member

  # @!attribute [r] element
  #   @return [HTMLUlElement] コンテナ要素（メンバー要素の入れ物）
  # @!attribute [r] objectName
  #   @return [String] オブジェクト名（対応する入力欄の`name`属性値の前半）

  constructor: (element)->
    @element = element
    @objectName = @element.dataset.objectName

  # @return [NodeList<HTMLInputElement>] メンバーに対応する入力欄要素群
  fields: ->
    @_container().querySelectorAll("[name^='#{@objectName}']")

  # @param [String] fieldName フィールド名
  # @return [HTMLInputElement] メンバーに対応する入力欄要素のうち`name`属性値の最後の階層が`fieldName`であるもの
  getField: (fieldName)->
    @_container().querySelector("[name='#{@objectName}[#{fieldName}]']")

  # @return [HTMLElement] メンバーに対応する入力欄が含まれうる要素
  _container: ->
    @element.closest(ActionField.SELECTORS.CONTAINER) || @element

  # メンバーを削除
  # @param [Hash] options オプション
  # @option options [Boolean] destroy レコードを削除するかどうか
  remove: (options = {})->

    # レコードの削除を有効にする処理
    if options.destroy
      if idField = @getField("id")
        @_container().appendChild(idField)
      if _destroyField = @getField("_destroy")
        @_container().appendChild(_destroyField)
        _destroyField.value = true

    # メンバー要素を削除
    if typeof jQuery == "function"
      $(@element).slideUp "fast", =>
        @element.remove()
    else
      @element.remove()

    # カスタムイベントを発火
    @element.dispatchEvent(ActionField.EVENTS.REMOVED)

  # 入力欄の値をコピー
  # @param [ActionField.Member] other コピー元メンバー
  inherit: (other)->
    Array.prototype.forEach.call other.fields(), (oldField)=>
      return if oldField.name.match(/\[id\]$/) || oldField.name.match(/\[_destroy\]$/)
      newName = oldField.name.replace(other.objectName, @objectName)
      if newField = @_container().querySelector("[name='#{newName}']")
        newField.value = oldField.value

  # @return [ActionField.Member] 自身をクローンしたメンバー
  clone: ->
    member = new ActionField.Member(@element.cloneNode(true))
    ["id", "_destroy"].forEach (fieldName)->
      if field = member.getField(fieldName)
        field.remove()
    member

  # 各属性値を置換
  # @param [String] newObjectName 置換後のオブジェクト名の前半部分
  replaceObjectName: (newObjectName)->
    # 属性`name`,`data-object-name`を置換
    oldObjectName = @objectName
    ["name", "data-object-name"].forEach (attr)=>
      @replaceAttributes(attr, oldObjectName, newObjectName)

    # 属性`id`,`for`を置換
    oldIdPrefix = ActionField.nameToId(oldObjectName)
    newIdPrefix = ActionField.nameToId(newObjectName)
    ["id", "for"].forEach (attr)=>
      @replaceAttributes(attr, oldIdPrefix, newIdPrefix)

    # インスタンス変数を書き換え
    @objectName = @element.dataset.objectName

  # 指定した属性の値を置換
  # @param [String] attr 属性名
  # @param [String] oldPrefix 置換前の属性値の前半部分
  # @param [String] newPrefix 置換後の属性値の前半部分
  replaceAttributes: (attr, oldPrefix, newPrefix)->
    regexp = new RegExp("^#{ActionField.escapeForRegExp(oldPrefix)}")
    if oldValue = @element.getAttribute(attr)
      @element.setAttribute(attr, oldValue.replace(regexp, newPrefix))
    elms = @_container().querySelectorAll("[#{attr}^='#{oldPrefix}']")
    Array.prototype.forEach.call elms, (elm)->
      elm.setAttribute(attr, elm.getAttribute(attr).replace(regexp, newPrefix))

  # メンバーの有効/無効化
  # @param [Boolean] bool `true`なら無効化、`false`なら有効化。
  disable: (bool)->
    @element.classList.toggle(ActionField.Member.MODE.DISABLED, bool)
    fields = @_container().querySelectorAll("[name^='#{@objectName}'],[for='#{@element.id}']")
    Array.prototype.forEach.call(fields, (field)-> field.disabled = bool)

ActionField.Member.MODE =
  # 無効状態
  DISABLED: "action_field-disabled"

# @param [String] template 要素のテンプレート
# @return [Array<ActionField.Member>] メンバーオブジェクトの配列
ActionField.Member.parse = (template)->
  parentElm = document.createElement('div')
  parentElm.innerHTML = template
  Array.prototype.map.call(parentElm.children, (elm)-> new ActionField.Member(elm))
