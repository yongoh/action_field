require 'rails_helper'

RSpec.describe Person, type: :model do
  let(:person){ create(:person) }

  describe "#name" do
    it{ expect(person.name).to match(/涼宮ハルヒ|キョン|長門有希|朝比奈みくる|古泉一樹/) }
  end

  describe "#height" do
    it{ expect(person.height).to be_between(150, 190).inclusive }
  end

  describe "#male" do
    it{ expect(person.male).to equal(true).or equal(false) }
  end

  describe "#birth" do
    it{ expect(person.birth).to be_between(Date.new(1950), Date.new(2020)).exclusive }
  end

  describe "#parent" do
    let(:person){ create(:person, :with_parent) }
    it{ expect(person.parent).to be_instance_of(Person) }
  end

  describe "#children" do
    let(:person){ create(:person, :with_children) }
    it{ expect(person.children).to all(be_instance_of(Person)).and have_attributes(size: 3) }
  end

  describe "#grandchildren" do
    let(:person){ create(:person, children: create_list(:person, 3, :with_children)) }
    it{ expect(person.grandchildren).to all(be_instance_of(Person)).and have_attributes(size: 9) }
  end
end
