require 'rails_helper'

context "単数関連先の入力欄が空の場合", js: true do
  before do
    visit new_person_path
    fill_in "person[name]", with: "織田信長"
  end

  subject do
    proc do
      sleep 0.5
      click_button "Create Person"
    end
  end

  shared_examples_for "create only one record" do
    scenario "は、関連元だけを新規作成すること" do
      expect(subject).to change{ Person.count }.by(1)
      expect(Person.all).to contain_exactly(
        have_attributes(parent_id: be_nil, name: "織田信長"),
      )
    end
  end

  shared_examples_for "successfully created" do
    scenario "は、更新成功の通知を表示すること" do
      subject.call
      expect(page).to have_selector("#notice", text: "Person was successfully created.")
    end
  end

  context "編集して送信した場合" do
    it_behaves_like "create only one record"
    it_behaves_like "successfully created"
  end

  context "「削除」イベントボタンをクリックして送信した場合" do
    before do
      within "#parent" do
        click_button "削除"
      end
    end

    it_behaves_like "create only one record"
    it_behaves_like "successfully created"
  end

  context "「新規作成」イベントボタンをクリックして" do
    before do
      within "#parent" do
        click_button "新規作成"

        fill_in "person[parent_attributes][name]", with: "織田信秀"
        fill_in "person[parent_attributes][height]", with: "156.3"
      end
    end

    context "送信した場合" do
      scenario "は、関連元と関連先を新規作成すること" do
        expect(subject).to change{ Person.count }.by(2)
        expect(Person.all).to contain_exactly(
          have_attributes(name: "織田信長", parent_id: be_present),
          have_attributes(name: "織田信秀", height: 156.3, parent_id: be_nil),
        )
      end

      it_behaves_like "successfully created"
    end

    context "「削除」イベントボタンをクリックして送信した場合" do
      before do
        within "#parent" do
          click_button "削除"
        end
      end

      it_behaves_like "create only one record"
      it_behaves_like "successfully created"
    end
  end
end
