describe "「全削除」イベントボタンをクリックした場合", ->
  beforeEach ->
    loadFixtures("container.html")
    appendLoadFixtures("singular_menu.html")
    @button = document.querySelector(EventButton.SELECTORS.button("action_field:clear"))

  describe "アニメーション終了後", ->
    beforeEach (done)->
      @button.click()
      setTimeout((-> done()), 500)

    it "は、コンテナに対応する全てのメンバー要素を削除すること", ->
      expect("#person_children_attributes-action_field-container").not.toContainElement(ActionField.SELECTORS.memberIn("person[children_attributes]"))

    it "は、コンテナに対応しないメンバー要素を削除しないこと", ->
      expect("#person_children_attributes-action_field-container").toContainElement("#animal_children_attributes_2-action_field-member")

    it "は、`id`および`_destroy`隠し入力欄を削除しないこと", ->
      expect("#person_children_attributes-action_field-container").toContainElement("#person_children_attributes_0_id")
      expect("#person_children_attributes-action_field-container").toContainElement("#person_children_attributes_0__destroy")
      expect("#person_children_attributes-action_field-container").toContainElement("#person_children_attributes_1_id")
      expect("#person_children_attributes-action_field-container").toContainElement("#person_children_attributes_1__destroy")

    it "は、`_destroy`隠し入力欄の値に'true'をセットすること", ->
      expect("#person_children_attributes_0__destroy").toHaveValue("true")
      expect("#person_children_attributes_1__destroy").toHaveValue("true")
