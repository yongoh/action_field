require 'rails_helper'

describe ActionField::Decorators::SingularEventButtonBuilder do
  let(:builder){ described_class.new(decorator) }
  let(:decorator){ double("FormDecorator", h: view_context) }

  describe "#create_button" do
    subject do
      builder.create_button("(´･ω･｀)")
    end

    before do
      allow(decorator).to receive(:container_id).and_return("my-container-id")
    end

    it "は、`template`データ属性を持つ「新規作成」イベントボタンを返すこと" do
      is_expected.to have_tag(
        ".event_button:not(.action_field-destroy_record)",
        with: {"data-event" => "action_field:create", for: "my-container-id", "data-template" => "(´･ω･｀)"},
        text: "新規作成",
      )
    end
  end

  describe "#remove_button" do
    subject do
      builder.remove_button
    end

    before do
      allow(decorator).to receive(:member_id).and_return("my-member-id")
    end

    it "は、「削除」イベントボタンを返すこと" do
      is_expected.to have_tag(
        ".event_button.action_field-destroy_record",
        with: {"data-event" => "action_field:remove", for: "my-member-id"},
        text: "削除",
      )
    end
  end
end
