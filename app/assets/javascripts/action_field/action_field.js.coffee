window.ActionField ?= {}

ActionField.SELECTORS =
  # コンテナ要素
  CONTAINER: ".action_field-container"
  # メンバー要素
  MEMBER: ".action_field-member"

  # @param [String] containerObjectName コンテナのオブジェクト名
  # @return [String] コンテナに対応するメンバー要素のセレクタ
  memberIn: (containerObjectName)->
    "#{ActionField.SELECTORS.MEMBER}[data-object-name^='#{containerObjectName}']"

# @param [String] str エスケープ前の文字列
# @return [String] エスケープ処理済みの正規表現用文字列
ActionField.escapeForRegExp = (str)->
  str.replace(/[-\/\\^$*+?.()|[\]{}]/g, "\\$&")

# @param [String] str '[',']'を含む文字列
# @return [String] スネークケース化した文字列
ActionField.nameToId = (str)->
  str.replace(/\[(.*?)\]/g, "_$1")

# @return [Number] 添字用のランダムな数値
ActionField.randomIndex = ->
  new Date().getTime()

# イベントオブジェクト群
ActionField.EVENTS =
  CREATED: new Event('action_field:created', bubbles: true, cancelable: true)
  REMOVED: new Event('action_field:removed', bubbles: true, cancelable: true)
