class ChangeMaleOnPeople < ActiveRecord::Migration[5.2]
  def change
    change_column_default :people, :male, true
  end
end
