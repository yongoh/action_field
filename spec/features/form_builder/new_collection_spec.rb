require 'rails_helper'

object_name = "person[children_attributes]"

context "複数関連先の入力欄が空の場合", js: true do
  before do
    visit new_person_path
    fill_in "person[name]", with: "織田信長"
  end

  subject do
    proc do
      sleep 0.5
      click_button "Create Person"
    end
  end

  shared_examples_for "create only one record" do
    scenario "は、関連元だけを追加すること" do
      expect(subject).to change{ Person.count }.by(1)
      expect(Person.all).to contain_exactly(
        have_attributes(parent_id: be_nil, name: "織田信長"),
      )
    end
  end

  shared_examples_for "successfully created" do
    scenario "は、更新成功の通知を表示すること" do
      subject.call
      expect(page).to have_selector("#notice", text: "Person was successfully created.")
    end
  end

  context "編集して送信した場合" do
    it_behaves_like "create only one record"
    it_behaves_like "successfully created"
  end

  context "「全削除」イベントボタンをクリックして送信した場合" do
    before do
      within "#children" do
        click_button "全削除"
      end
    end

    it_behaves_like "create only one record"
    it_behaves_like "successfully created"
  end

  context "「追加」イベントボタンをクリックして" do
    before do
      within "#children" do
        click_button "追加"

        object_name = find(".action_field-member:last-child")["data-object-name"]
        fill_in "#{object_name}[name]", with: "羽柴秀勝"
        fill_in "#{object_name}[height]", with: "156.4"
      end
    end

    context "送信した場合" do
      scenario "は、関連元と関連先を追加すること" do
        expect(subject).to change{ Person.count }.by(2)
        expect(Person.all).to contain_exactly(
          have_attributes(name: "織田信長", parent_id: be_nil),
          have_attributes(name: "羽柴秀勝", height: 156.4, parent_id: be_present),
        )
      end

      it_behaves_like "successfully created"
    end

    context "追加したメンバーを削除して送信した場合" do
      before do
        within "#children .action_field-member:last-child" do
          click_button "削除"
        end
      end

      it_behaves_like "create only one record"
      it_behaves_like "successfully created"
    end
  end
end
