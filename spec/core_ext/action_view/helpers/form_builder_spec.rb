require 'rails_helper'

describe ActionView::Helpers::FormBuilder do
  let(:form_builder){ h.form_for(person){|f| return f } }

  describe "#action_field" do
    let(:member_proc){
      proc do |f|
        h.concat h.content_tag(:span, "(´･ω･｀)", class: "shobon")
        h.concat f.text_field(:name)
        h.concat f.number_field(:height)
        h.concat f.remove_button
      end
    }

    let(:template){ subject.slice(/data-template="(.+?)"/, 1).gsub("&quot;", "'") }
    let(:new_person){ build(:person) }

    shared_examples_for "with template event_button" do
      it "は、新規メンバー用テンプレートを持つイベントボタンを含むHTMLを返すこと" do
        expect(template).to have_tag(".action_field-member", with: {"data-object-name" => object_name}, count: 1){
          without_tag("input", with: {type: "hidden"})
          with_tag("span.shobon", text: "(´･ω･｀)")
          with_text_field("#{object_name}[name]", new_person.name)
          with_number_field("#{object_name}[height]", new_person.height)
          with_tag(".event_button", with: {"data-event" => "action_field:remove", for: "#{h.send(:sanitize_to_id, object_name)}-action_field-member"})
        }
      end
    end

    context "複数関連の名前を渡した場合" do
      subject do
        form_builder.action_field(:children, &member_proc)
      end

      let(:person){ create(:person, :with_children) }

      it "は、関連先と同じ数のメンバー要素を含むコンテナを返すこと" do
        is_expected.to have_tag(
          "ul.action_field-container#person_children_attributes-action_field-container",
          with: {"data-object-name" => "person[children_attributes]"},
        ){
          person.children.each_with_index do |child, i|
            with_tag(
              "li.action_field-member#person_children_attributes_#{i}-action_field-member",
              with: {"data-object-name" => "person[children_attributes][#{i}]"},
              count: 1,
            ){
              with_hidden_field("person[children_attributes][#{i}][id]", child.id)
              with_hidden_field("person[children_attributes][#{i}][_destroy]", "false")
              with_tag("span.shobon", text: "(´･ω･｀)")
              with_text_field("person[children_attributes][#{i}][name]", child.name)
              with_number_field("person[children_attributes][#{i}][height]", child.height)
              with_tag(".event_button", with: {"data-event" => "action_field:remove", for: "person_children_attributes_#{i}-action_field-member"})
            }
          end

          with_tag(".action_field-member", count: person.children.size)
        }
      end

      it "は、複数コンテナ用のイベントボタンを含むメニュー要素を返すこと" do
        is_expected.to have_tag("menu.action_field-menu#person_children_attributes-action_field-menu"){
          with_tag(".event_button[data-template]", with: {"data-event" => "action_field:add"})
          with_tag(".event_button", with: {"data-event" => "action_field:clear"})
        }
      end

      let(:object_name){ "person[children_attributes][#{person.children.size}]" }

      describe "オプション`:templates`" do
        context "を渡さない場合" do
          let(:new_person){ Person.new }
          it_behaves_like "with template event_button"
        end

        context "にレコードの配列を渡した場合" do
          subject do
            form_builder.action_field(:children, templates: [new_person], &member_proc)
          end

          it_behaves_like "with template event_button"
        end
      end
    end

    context "単数関連の名前を渡した場合" do
      subject do
        form_builder.action_field(:parent, &member_proc)
      end

      let(:person){ create(:person, :with_parent) }

      it "は、関連先の値を持つメンバー要素を含むコンテナを返すこと" do
        is_expected.to have_tag(
          "ul.action_field-container#person_parent_attributes-action_field-container",
          with: {"data-object-name" => "person[parent_attributes]"},
        ){
          with_tag("li.action_field-member#person_parent_attributes-action_field-member", with: {"data-object-name" => "person[parent_attributes]"}, count: 1){
            with_hidden_field("person[parent_attributes][id]", person.parent.id)
            with_hidden_field("person[parent_attributes][_destroy]", "false")
            with_tag("span.shobon", text: "(´･ω･｀)")
            with_text_field("person[parent_attributes][name]", person.parent.name)
            with_number_field("person[parent_attributes][height]", person.parent.height)
            with_tag(".event_button", with: {"data-event" => "action_field:remove", for: "person_parent_attributes-action_field-member"})
          }
        }
      end

      it "は、単数コンテナ用のイベントボタンを含むメニュー要素を返すこと" do
        is_expected.to have_tag("menu.action_field-menu#person_parent_attributes-action_field-menu"){
          with_tag(".event_button[data-template]", with: {"data-event" => "action_field:create"})
          with_tag(".event_button", with: {"data-event" => "action_field:remove"})
        }
      end

      let(:object_name){ "person[parent_attributes]" }

      describe "オプション`:templates`" do
        context "を渡さない場合" do
          let(:new_person){ Person.new }
          it_behaves_like "with template event_button"
        end

        context "にレコードの配列を渡した場合" do
          subject do
            form_builder.action_field(:parent, templates: [new_person], &member_proc)
          end

          it_behaves_like "with template event_button"
        end
      end
    end
  end
end
