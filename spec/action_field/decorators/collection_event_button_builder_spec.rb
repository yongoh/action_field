require 'rails_helper'

describe ActionField::Decorators::CollectionEventButtonBuilder do
  let(:builder){ described_class.new(decorator) }
  let(:decorator){ double("FormDecorator", h: view_context, container_id: "my-container-id") }

  describe "#add_button" do
    subject do
      builder.add_button("(´･ω･｀)")
    end

    it "は、`template`データ属性を持つ「追加」イベントボタンを返すこと" do
      is_expected.to have_tag(
        ".event_button:not(.action_field-destroy_record)",
        with: {"data-event" => "action_field:add", for: "my-container-id", "data-template" => "(´･ω･｀)"},
        text: "追加",
      )
    end
  end

  describe "#clear_button" do
    subject do
      builder.clear_button
    end

    it "は、「全削除」イベントボタンを返すこと" do
      is_expected.to have_tag(
        ".event_button.action_field-destroy_record[data-confirm]",
        with: {"data-event" => "action_field:clear", for: "my-container-id"},
        text: "全削除",
      )
    end
  end
end
